jQuery(document).ready(function () {
	if (jQuery.browser.msie) {         //this is for only ie condition ( microsoft internet explore )
		jQuery('input[placeholder], textarea[placeholder]').each(function () {
			var obj = jQuery(this);

			if (obj.attr('placeholder') != '') {
				obj.addClass('IePlaceHolder');

				/*if (jQuery.trim(obj.val()) == '' && obj.attr('type') != 'password') {
					obj.val(obj.attr('placeholder'));
				}*/
				if (jQuery.trim(obj.val()) == '') {
					obj.val(obj.attr('placeholder'));
				}
			}
		});

		jQuery('.IePlaceHolder').focus(function () {
			var obj = jQuery(this);
			if (obj.val() == obj.attr('placeholder')) {
				obj.val('');
			}
		});

		jQuery('.IePlaceHolder').blur(function () {
			var obj = jQuery(this);
			if (jQuery.trim(obj.val()) == '') {
				obj.val(obj.attr('placeholder'));
			}
		});
	}
});