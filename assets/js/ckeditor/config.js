/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ){
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E'; 
	
	// Link dialog, "Browse Server" button
		config.filebrowserBrowseUrl = 'http://192.168.0.209/projects/spaceo/tomouh/assets/js/ckeditor/plugins/ckfinder/ckfinder.html';
		// Image dialog, "Browse Server" button
		config.filebrowserImageBrowseUrl = 'http://192.168.0.209/projects/spaceo/tomouh/assets/js/ckeditor/plugins/ckfinder/ckfinder.html?type=Images';
		// Flash dialog, "Browse Server" button
		config.filebrowserFlashBrowseUrl = 'http://192.168.0.209/projects/spaceo/tomouh/assets/js/ckeditor/plugins/ckfinder/ckfinder.html?type=Flash';
		// Upload tab in the Link dialog
		config.filebrowserUploadUrl = 'http://192.168.0.209/projects/spaceo/tomouh/assets/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
		// Upload tab in the Image dialog
		config.filebrowserImageUploadUrl = 'http://192.168.0.209/projects/spaceo/tomouh/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
		// Upload tab in the Flash dialog
		config.filebrowserFlashUploadUrl = 'http://192.168.0.209/projects/spaceo/tomouh/assets/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

		config.allowedContent= true;
	
		config.extraAllowedContent = 'a button';

	config.font_names = 'ProximaNova Regular/ProximaNova_Regular;' + config.font_names;
	
};