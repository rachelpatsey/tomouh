<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



require_once('./zoho/vendor/autoload.php');

require ('application/stripe/config.php');



use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./application/helpers/general_helper.php');

// require_once('./application/mailchimp/MCAPI.class.php');

require_once('./zoho_v2/vendor/autoload.php');


class Payment extends CI_Controller

{

	public function __construct(){

		parent::__construct();



		$this->load->model('admin/admin_model');

		$this->load->model('tomouh_model');

		$this->load->model('messages_model');

		$this->load->library('session');

		$this->load->library('form_validation');

		ZCRMRestClient::initialize();

	}

	public function index(){

		$this->load->helper('url');

		$this->session->unset_userdata('logged_user');
		
		$data['meta_title'] = 'Payment';

		$data['meta_keyword'] = 'Payment';

		$data['meta_description'] = 'Payment';
		
		$user_session = $this->session->userdata["user"];
		
		if(!isset($this->session->userdata["user"])){
			redirect(base_url().'signup');
			exit;
		}

	    $secret_key = $this->tomouh_model->getSetting('SECRET_KEY');
		$public_key = $this->tomouh_model->getSetting('PUBLIC_KEY');
		
	    $stripe = array(

		  "secret_key"      => $secret_key,

		  "publishable_key" => $public_key,

		);



		\Stripe\Stripe::setApiKey($stripe['secret_key']);



		if($this->input->post()){



				  $card_name = $_POST['cc_owner'];

				  $card_number = $_POST['cc_number'];

				  $card_exp_month = $_POST['cc_expire_date_month'];

				  $card_exp_year = $_POST['cc_expire_date_year'];

				  $card_cvv = $_POST['cc_cvv2'];



				  $user_data = $this->session->userdata('user');



				  $plan = $this->tomouh_model->getPlan();



				  $plan_id = $plan['v_plan_id'];



				  $user_email = $user_data['email'];



				   try{



				   $result = \Stripe\Token::create(

				                    array(

				                        "card" => array(

				                            "name" => $card_name,

				                            "number" => $card_number,

				                            "exp_month" => $card_exp_month,

				                            "exp_year" => $card_exp_year,

				                            "cvc" => $card_cvv

				                        )

				                    )

				                );

				   $token = $result['id'];



					}catch(Exception $e){



						$error = $e->getMessage();

						redirect(base_url().'payment?succ=0&msg='.$error);

						exit;

					}



					try{

				    $customer = \Stripe\Customer::create([

								    'email' => $user_email,

								    'source' => $token,

								]);



					}catch(Exception $e){



						$error = $e->getMessage();

						redirect(base_url().'payment?succ=0&msg='.$error);

						exit;

					}



					try{

				   $subscription = \Stripe\Subscription::create([

					    'customer' => $customer->id,

					    'items' => [['plan' => $plan_id]],

					]);

				    

					}catch(Exception $e){



						$error = $e->getMessage();

						redirect(base_url().'payment?succ=0&msg='.$error);

						exit;

					}



					



				    if($subscription){



				    	$subscription_id = $subscription->id;

						$customer_id =$customer->id;

				    	$next_subscription_date = date('Y-m-d', strtotime('+1 year'));  





				    	$user_data = $this->session->userdata('user');



				    	$address = $user_data['residence_city'].' '.$user_data['residence_country'];



				    	$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');



						// $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

						

						$geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

						

						$geo = json_decode($geo, true);



						if (isset($geo['status']) && ($geo['status'] == 'OK')) {



						  $latitude = $geo['results'][0]['geometry']['location']['lat']; 

						  $longitude = $geo['results'][0]['geometry']['location']['lng']; 



						}else{



							$latitude = ''; 

						  	$longitude = ''; 

						}



				    	$data = $this->session->userdata('user_step2');



				    	$main_occupation_description = isset($data['main_occu'])?$data['main_occu']:'';



				    	$main_occupation_data = isset($data['occupation'])?$data['occupation']:'';

						

						$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);



						$passion_interest = $data['passions_interests'];



						$description = $data['description'];



						if($user_data['referral'] == 0){



							$referral_email = '';

						}else{



							$referral_email = $user_data['referral_email'];

						}



						/*

						email part	

						*/

						

						$user = $user_data['email'];



						$email_to = $user;

						$email_from = "";



						$template = $this->tomouh_model->getEmailTemplate(2);

						$email_subject = $template['v_subject'];

						$content = $template['l_body'];

						

						$activation_link = '<a href="'.base_url().'login/activation/'.md5($user_data['email']).'" target="_blank">link here</a>';

				

						$content = str_replace("link here", $activation_link, $content);



						$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );



						// $sent = 1;



						if($sent){



						$insert_data = array(



							'v_email'   	 => $user_data['email'],

							'v_password' 	 => md5($user_data['password']),

							'v_firstname'	 => $user_data['firstname'],

							'v_lastname' 	 => $user_data['lastname'],

							'd_dob'      	 => date('Y-m-d', strtotime($user_data['dob'])),

							'e_gender'    	 => $user_data['gender'],

							'v_home_country' => $user_data['home_country'],

							'v_home_city'    => $user_data['home_city'],

							'v_residence_country' => $user_data['residence_country'],

							'v_residence_city'    => $user_data['residence_city'],

							'v_telephone' 	 => $user_data['tele_country_code'].'-'.$user_data['telephone'],

							'v_telephone_2'  => $user_data['tele_2_country_code'].'-'.$user_data['telephone_2'],

							'v_twitter_link '=> $user_data['twitter_link'],

							'v_linkedin_link'=> $user_data['linkedin_link'],

							'e_referral' 	 => $user_data['referral'],

							'v_referral_email' => $referral_email,

							't_passion_interest'  => isset($passion_interest)?$passion_interest:'',

							't_description'  => isset($description)?$description:'',

							'v_main_occupation' => isset($main_occupation_description)?$main_occupation_description:'',

							'v_longitude'=>$longitude,

							'v_latitude'=>$latitude,

							'e_status'=>'inactive',

							'e_member_of_month'=>0,

							'v_subscription_id'=>$subscription_id,

							'e_type'=>$user_data['type'],

							'e_plan_type'=>'paid',
							
							'v_label' => 'Member',

							'v_crm_contact_id'=>$user_data['contact_id'],

							'e_payment_type'=>'stripe',

							'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year')),

						);



						$tbl = "tbl_members";

						$add_user_data = $this->admin_model->add_entry($insert_data,$tbl);



						$this->add_zoho_crm($add_user_data);



						if($user_data['type'] == 'member'){



							$this->add_mailchimp($user_data['email'],$user_data['firstname'],$user_data['lastname']);

						}



						$subscription_data = array(



							'user_id'=>$add_user_data,

							'v_subscription_id'=>$subscription_id,

							'v_customer_id'=>$customer_id,

							'l_subscription_data'=>$subscription,

							'd_subscription_date'=>date("Y-m-d H:i:s"),

							'e_payment_type'=>'stripe',

								);



						$this->admin_model->add_entry($subscription_data,"tbl_subscription_data");



						$educ_data_length = sizeof($data['university']);



						for($i=0;$i<$educ_data_length;$i++){

						  if(isset($data['university'][$i]) && !empty($data['university'][$i])){	

							if($int_main_occupation <= 10){



								if(($i+1) == $int_main_occupation){

									$main_occupation = 1;									

								}else{

									$main_occupation = 0;

								} 

							}else{

								$main_occupation = 0;

							}



							$insert_array = array(



								'user_id'=>$add_user_data,

								'v_university' =>$data['university'][$i],

								'v_degree'=>$data['degree'][$i],

								'v_major'=>$data['major'][$i],

								'i_passing_year'=>$data['graduation_year'][$i],

								'i_main_occupation'=>$main_occupation

							);

							$tbl="tbl_educational_data";

							$this->admin_model->add_entry($insert_array,$tbl);

						  }

						}



						$company_data_length = sizeof($data['company']);



						for($i=0;$i<$company_data_length;$i++){



						 if(isset($data['company'][$i]) && !empty($data['company'][$i])){

							if($int_main_occupation >= 10){



								if(($i+11) == $int_main_occupation){

									$main_occupation = 1;									

								}else{

									$main_occupation = 0;

								} 

							}else{	

								$main_occupation = 0;

							}



							$insert_array = array(



								'user_id'=>$add_user_data,

								'v_company' =>$data['company'][$i],

								'v_job_title'=>$data['job_title'][$i],

								'v_industry'=>$data['industry'][$i],

								'i_company_from'=>$data['company_from'][$i],

								'i_company_to'=>$data['company_to'][$i],

								'i_main_occupation'=>$main_occupation

							);

							$tbl="tbl_professional_data";

							$this->admin_model->add_entry($insert_array,$tbl);

						  }	

						}



						$award_data_length = sizeof($data['award_details']);



						for($i=0;$i<$award_data_length;$i++){



						   if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){

							$insert_array = array(



								'user_id'=>$add_user_data,

								'v_achievement' =>$data['award_details'][$i],

							);

							$tbl="tbl_achievements_data";

							$this->admin_model->add_entry($insert_array,$tbl);



							// $member_update_array = array(



							// 			'user_id'=>$add_user_data,

							// 			'v_user_name'=>$user_data['firstname'].' '.$user_data['lastname'],

							// 			'l_description' =>'has Become the '.$data['award_details'][$i].'.',

							// 			'd_added'=>date("Y-m-d H:i:s"),

							// 			); 

							

							// $this->admin_model->add_entry($member_update_array,"tbl_members_updates");	

						   }							

						}

							$res = setcookie('user_step1', '', time() - 2592000, '/');



	                        $res = setcookie('user_step2', '', time() - 2592000, '/');



							$this->session->unset_userdata('user');

							$this->session->unset_userdata('user_step2');



							redirect(base_url().'payment_success');

				  			exit();





						}else{



							redirect(base_url().'payment?succ=0&msg=error');

							exit;



							}

				    }				    

				  

				  redirect(base_url().'payment_success');

				  exit();

		}
		
		if($user_session['email'] != ''){
			$user_info = $this->tomouh_model->getUserByMd5Email(md5($user_session['email']));
			
			if($user_info['d_subscription_exp_date'] != '' && $user_info['d_subscription_exp_date'] != '0000-00-00'){
				
				$data['expired_on'] = $user_info['d_subscription_exp_date'];
			}
			
		}
		
		//print_r($data);
		//exit;
		
		$this->load->view('payment', $data);

	}



	public function add_zoho_crm($user_id){



		$member_data = $this->tomouh_model->getUserByUserId($user_id);



		$payment_data = 'Payment Type : '.$member_data['e_payment_type'].', Subscription Id : '.$member_data['v_subscription_id'].', Activation Date : '.date('Y-m-d').', Expiration Date : '.$member_data['d_subscription_exp_date'];



		$user_step1 = $this->session->userdata('user');



		$user_step2 = $this->session->userdata('user_step2');



		if(isset($user_step2['university']) && !empty($user_step2['university'])){



            $educ_data_length = sizeof($user_step2['university']);



            $education_info = array();



            for($i=0;$i<$educ_data_length;$i++){



                if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){



                $education_info[] = 'University : '.$user_step2['university'][$i].', Degree : '.$user_step2['degree'][$i].', Major : '.$user_step2['major'][$i].', Year : '.$user_step2['graduation_year'][$i];



                }

            }

        }



        if(isset($user_step2['company']) && !empty($user_step2['company'])){



            $company_data_length = sizeof($user_step2['company']);



            $experience_info = array();



            for($i=0;$i<$company_data_length;$i++){



                if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){



                $experience_info[] = 'Company : '.$user_step2['company'][$i].', Job Title : '.$user_step2['job_title'][$i].', Industry : '.$user_step2['industry'][$i].', Period : '.$user_step2['company_from'][$i].'-'.$user_step2['company_to'][$i];



                }

            }

        }



        if(isset($user_step2['award_details']) && !empty($user_step2['award_details'])){



            $award_data_length = sizeof($user_step2['award_details']);



            $award_detail = '';



            for($i=0;$i<$award_data_length;$i++){



                if(isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])){



                 $award_detail .= $user_step2['award_details'][$i].',';



                }

            }

        }



        if(isset($award_detail)){



            $AAH = rtrim($award_detail,',');



        }else{

            $AAH = '';

        }



        $educational_info = implode(' %0A ',$education_info);



        $exp_info = implode(' %0A ', $experience_info);

		// insert into member
		
		//to get the instance of the module
		// $moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

		// $recordsMember=array();
		// //To get ZCRMRecord instance
		// $recordMember=ZCRMRecord::getInstance("Members",null); 
		// //This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		// $recordMember->setFieldValue("Last_Name",$user_step1['lastname']);
		// $recordMember->setFieldValue("Email",$user_step1['email']);
		// $recordMember->setFieldValue("Name",$user_step1['firstname']);
		// $recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		// $recordMember->setFieldValue("Gender",$user_step1['gender']);
		// $recordMember->setFieldValue("Expired_On","");
		// $recordMember->setFieldValue("Home_Country",$user_step1['home_country']);
		// $recordMember->setFieldValue("Home_City",$user_step1['home_city']);
		// $recordMember->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		// $recordMember->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		// $recordMember->setFieldValue("City_Of_Residence'",$user_step1['residence_city']);
		// $recordMember->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone']);
		// $recordMember->setFieldValue("Educational_Info_1.",isset($educational_info)? urldecode($educational_info):''); 
		// $recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
		// $recordMember->setFieldValue("AAH",$AAH);
		// $recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		// $recordMember->setFieldValue("Subscription_Info","");
		// $recordMember->setFieldValue("Status'","Active");
		// $recordMember->setFieldValue("Plan","Paid");
		// $recordMember->setFieldValue("Label","Member");

		// array_push($recordsMember, $recordMember);//pushing the record to the array 
		// $responseIn=$moduleMemberIns->createRecords($recordsMember);
		// $responseIn = $responseIn->getEntityResponses();
		// $responseIn = $responseIn[0]->getDetails();
		// $responseCrmID = $responseIn['id'];  
		
		// $update_array  = array(

		// 		'v_member_crm_id'=>$responseCrmID,

		// 		);

		// $this->admin_model->update_entry($user_id,$update_array,"tbl_members");

		$contact_id = $user_step1['contact_id'];

		$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
		$zcrmRecordIns->setFieldValue("Label", "Member");

		$zcrmRecordIns->setFieldValue("Last_Name",$user_step1['lastname']);
		$zcrmRecordIns->setFieldValue("Email",$user_step1['email']);
		$zcrmRecordIns->setFieldValue("First_Name",$user_step1['firstname']);
		$zcrmRecordIns->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		$zcrmRecordIns->setFieldValue("Gender",$user_step1['gender']);
		$zcrmRecordIns->setFieldValue("Profile_Expiry","");
		$zcrmRecordIns->setFieldValue("Country",$user_step1['home_country']);
		$zcrmRecordIns->setFieldValue("Home_City",$user_step1['home_city']);
		$zcrmRecordIns->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		$zcrmRecordIns->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		$zcrmRecordIns->setFieldValue("City_Of_Residence",$user_step1['residence_city']);
		$zcrmRecordIns->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone']);
		$zcrmRecordIns->setFieldValue("Educational_Info",isset($educational_info)? urldecode($educational_info):''); 
		$zcrmRecordIns->setFieldValue("Experience_Info",isset($exp_info)? urldecode($exp_info):'');
		$zcrmRecordIns->setFieldValue("AAH",$AAH);
		$zcrmRecordIns->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		$zcrmRecordIns->setFieldValue("Subscription_Info","");
		$zcrmRecordIns->setFieldValue("Member_Status","Active");
		$zcrmRecordIns->setFieldValue("Plan","Paid");
		$zcrmRecordIns->setFieldValue("Label","Member");
		$apiResponse   =$zcrmRecordIns->update();
	
	}



	public function webhook(){


		$input = file_get_contents("php://input");



	    $subscription_json = json_decode($input);



	    if(empty($subscription_json)){



	    	exit();

	    }

	 

	    $subscription_data = $subscription_json->data;



	    $subscription_object = $subscription_data->object;



	    $subscription_id = $subscription_object->id;



		$member = $this->tomouh_model->getUserBySubscriptionId($subscription_id);	



		if(isset($member) && !empty($member)){



			$member_id = $member['id'];



			$this->update_zoho_crm($member_id);



		    $subscription_array = array(

		    		'user_id'=>$member_id,

		    		'v_customer_id'=>$subscription_object->customer,

		    		'v_subscription_id'=>$subscription_id,

		    		'l_subscription_data'=>$subscription_json,

		    		'd_subscription_date'=>date("Y-m-d H:i:s"),

		    		'e_payment_type'=>'stripe',

		    	);

		    $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");



		    $update_member = array(

		    		'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year')),

		    		);



		    $this->admin_model->update_entry($member_id,$update_member,"tbl_members");

		}

	}



	public function update_zoho_crm($user_id){
		$results = $this->tomouh_model->getUserByUserId($user_id);
		
		$contact_crm_id = $results['v_crm_contact_id'];

		$zcrmModuleIns = ZCRMModule::getInstance("Contacts");
		$bulkAPIResponse=$zcrmModuleIns->getRecord($contact_crm_id);
		$recordData = $bulkAPIResponse->getData();
		$mapData=$recordData->getData();
		$payment_data = $mapData['Subscription_Info'];
		
		
		$payment_new_data = $payment_data.' %0A Payment Type : '.$results['e_payment_type'].', Subscription Id : '.$results['v_subscription_id'].', Expiration Date : '.date('Y-m-d',strtotime('+1 year'));

		// echo "<pre>"; print_r($payment_new_data); exit;
		
		$zcrmRecordMemIns = ZCRMRecord::getInstance("Contacts", $contact_crm_id);
		$zcrmRecordMemIns->setFieldValue("Profile_Expiry", date('Y-m-d',strtotime('+1 year')));
		$zcrmRecordMemIns->setFieldValue("Subscription_Info", urldecode($payment_new_data));
		$apiResponse=$zcrmRecordMemIns->update();
	}

	public function add_mailchimp($email,$firstname,$lastname){

		// mailchimp api call to subscribe start

		$merge_vars = array(
                'EMAIL' => $email,
                'FNAME' => $firstname,
                'LNAME' => $lastname,
            );

		mailChimpSubscribe($merge_vars);
		
		// mailchimp api call to subscribe end	

		
	}

}