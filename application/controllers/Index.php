<?php
use CristianPontes\ZohoCRMClient\Exception\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho_v2/vendor/autoload.php');
// require_once('./application/mailchimp/MCAPI.class.php');
require_once('./application/helpers/general_helper.php');

class Index extends CI_Controller

{

	public function __construct(){

		parent::__construct();

		$this->load->model('admin/admin_model');

		$this->load->model('tomouh_model');

		$this->load->model('messages_model');

		$this->load->library('session');

		$this->load->library('form_validation');
		ZCRMRestClient::initialize();


	}

	public function index(){

		// echo "ee"; exit;
		$this->load->helper('url');
		$data = array();
		$code = $_GET['code']; 
		if($code != ''){
			$user = $this->tomouh_model->getUserByMd5Email($code);
			$user_id = $user['id'];
			
			$update_user = array(
				'e_updated' => 'yes',
				'd_last_profile_update'=>date('Y-m-d'),
			);
	
			$tbl = "tbl_members";
			$this->admin_model->update_entry($user_id, $update_user, $tbl);

			$contact_id = $user['v_crm_contact_id'];

			//UPDATE CONTACT DATA
			try{
				$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

				$zcrmRecordContactIns->setFieldValue("Last_profile_update", date('Y-m-d'));
	
				// $apiResponse = $zcrmRecordContactIns->update();

			}catch(Exception $e){

			}
		
			$result = $this->tomouh_model->getPageBySlug('thank-you');
			$data['main_description'] = $result['l_description'];

			$this->load->view('profile_upto_date',$data);
			// redirect(base_url().'profile_upto_date');
			// exit;

		}else{

			



			$result = $this->tomouh_model->getPageBySlug('home');
	
			$data = $result;
	
			$data['meta']= $this->tomouh_model->getPageMeta($result['id']);
	
			$data['meta_title'] = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
	
			$data['meta_keyword'] = $result['v_meta_keyword'];
	
			$data['meta_description'] = $result['l_meta_description'];
	
			$data['main_description'] = $result['l_description'];
	
			$data['body_class'] = 'home page-'.$result['id'];
	
			$data['title'] = $result['v_title'];
	
			
	
			$data['total_member'] = $this->tomouh_model->getTotalMembers();
	
	
	
			$data['sliders'] = $this->tomouh_model->getSlider();
	
	
	
			$data['books'] = $this->tomouh_model->getBooks();
	
	
	
			$members = $this->tomouh_model->getMembers();
	
	
	
			$countries = $this->tomouh_model->getCountry();
	
	
	
			$country_done= array();
	
			foreach ($members as $member) {
	
				if(isset($member['v_residence_country']) && !empty($member['v_residence_country'])){
	
				
	
					if($member['v_residence_country'] !='Saudi Arabia' && !in_array($member['v_residence_country'], $country_done)){
	
						$total_member = $this->tomouh_model->getMembersByCountry($member['v_residence_country']);
	
						
	
						if(($member['v_longitude']) && ($member['v_latitude'])){
	
							$data_array = array(
	
								'latitude' => $member['v_latitude'],
	
								'longitude' => $member['v_longitude'],
	
								'country_name'=> $member['v_residence_country'],
	
								'total_members'=>$total_member
	
							);
	
							$lan_lat[] = $data_array;
	
							$country_done[]= $member['v_residence_country'];
	
						}
	
					}
	
				}
	
			}
	
		$western_region = array('Jeddah','Makkah AL-Mukkaramah','Makkah','Mecca','AL-Madinah Al-Munawarah','Almadina','Yanbu','Tabouk','Taif','Abha','Al Baha','Jizan','Jubail','Bisha','Najran','KAEC','KAUST','King Abdullah Economic City','KhamisMushayt','Rabigh','Sabt Al Alaya','Thuwal');
	
		$central_region = array('Riyadh','Al Qaseem','Hail','Buraydah','Diriyah',"Ha'il",'Jouf','Al Jawf','Sakakah');
	
		$eastern_region = array('Dammam','Al-Khobar','Dhahran','Al-Ahsa','Qatif','Hofuf','Hafr Al-Batin','Jubail');
	
			
	
		$total_western = $this->tomouh_model->getMembersByCountryCity('Saudi Arabia', $western_region);
	
		$total_central = $this->tomouh_model->getMembersByCountryCity('Saudi Arabia', $central_region);
	
		$total_eastern = $this->tomouh_model->getMembersByCountryCity('Saudi Arabia', $eastern_region);
	
		/*echo "total_western".$total_western;
		echo "<br>total_central".$total_central;
		echo "<br>total_eastern".$total_eastern;
		exit;*/
	
		if($total_western > 0){
	
			$lan_lat[] = array(
	
							'latitude' => '21.5934402',
	
							'longitude' => '39.1484342',
	
							'country_name'=> 'Western Region',
	
							'total_members'=>$total_western
	
						);
	
		}
	
		
	
		if($total_central > 0){
	
			$lan_lat[] = array(
	
							'latitude' => '24.7311382',
	
							'longitude' => '46.6678927',
	
							'country_name'=> 'Central Region',
	
							'total_members'=>$total_central
	
						);
	
		}
	
		
	
		if($total_eastern > 0){
	
			$lan_lat[] = array(
	
							'latitude' => '26.3719063',
	
							'longitude' => '50.1046252',
	
							'country_name'=> 'Eastern Region',
	
							'total_members'=>$total_eastern
	
						);
	
		}			
	
			
	
			//echo "<pre>"; print_r($lan_lat);
	
			//exit;
	
				
	
			// foreach($members as $member){
	
	
	
		 //      	if(($member['v_longitude']) && ($member['v_latitude'])){
	
	
	
		 //      	$data_array = array(
	
			//         'latitude' => $member['v_latitude'],
	
			//         'longitude' => $member['v_longitude'],
	
			//         'name' => $member['v_firstname'].' '.$member['v_lastname'],
	
			//         'city' =>  $member['v_residence_city'],
	
			//     );
	
		 //      	$lan_lat[] = $data_array;
	
		 //      	}
	
		 //    }
	
			$data['lan_lat'] = $lan_lat;
	
	
	
			$data['members_update'] = $this->tomouh_model->getMembersUpdate();
	
	
	
			$data['testimonial'] = $this->tomouh_model->getTestimonial();
	
	
	
			$data['member_of_month'] = $this->tomouh_model->getMemberOfMonth();
	
	
	
			$data['google_map_key'] = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');	   
	
	
	
			$this->load->view('index',$data);

		}
		
		
	

	}

}