<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Event extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('admin/admin_model');
	}
	public function index(){
		
		$data['meta_title'] = 'Events';
		$data['meta_keyword'] = 'Events';
		$data['meta_description'] = 'Events';
			
		$this->load->helper('url');

		if($this->input->post()){

			if($this->input->post('send_mail') == 'send_mail'){
				// echo "dd"; exit;

				// $site_email = $this->admin_model->getSetting('CONTACT_INFO');
				$email_to 	= $this->admin_model->getSetting('EVENT_RSVP_EMAIL_TO');
				$user_id 	= $this->session->userdata('logged_user');
				$user_data  = $this->tomouh_model->getUserByUserId($user_id);
				$email_subject = $this->input->post('subject');
				$email_subject = $email_subject.' by '.$user_data['v_firstname'].' '.$user_data['v_lastname'];
				$content 	= $this->input->post('message');
				// print_r($user_data['v_firstname']); exit;
				// $to_id 		= $this->tomouh_model->getUserByEmail($email_to);
				
				$email_from = $user_data['v_email'];
				// $email_from = 'victoriahale.cis@gmail.com';
				
				$config = array(
				  'mailtype' => 'html',
				);
				// $email_to = 'victoriahale.cis@gmail.com';
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from($email_from);
				$this->email->to($email_to);
				// $this->email->cc($site_email);
				$this->email->subject($email_subject);
				$this->email->message($content);

				// echo "<pre>";
				// print_r($this->email);
				// exit;
				$sent = $this->email->send();
				
				redirect(base_url().'event?succ=1&msg=send_rsvp_msg');
				exit();
			}
		}
		
		$this->load->view('event', $data);
	}
}