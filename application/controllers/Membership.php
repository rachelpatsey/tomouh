<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Membership extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){

		$this->load->helper('url');

		$data = array();
		$data['meta_keyword'] = 'Member Updates';
		$data['meta_description'] = 'Member Updates';
		$data['meta_title'] = 'Member Updates';

		if($this->input->post()){
			
			$data['members_update'] = $this->tomouh_model->getMembersUpdate();

			$this->load->view('membership',$data);
		
			
		}else{

			$members = array();
			$members_update = $this->tomouh_model->getMembersUpdate();

			for($i=0;$i<8;$i++){
				$members[] = $members_update[$i];
			}

			$data['members_update'] = $members;

			$this->load->view('membership',$data);

		}
	}
}