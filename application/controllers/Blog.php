<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog extends CI_Controller
{
	public function __construct(){
		parent::__construct();	
		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){
		
		$data['meta_title'] = 'Blog';
		$data['meta_keyword'] = 'Blog';
		$data['meta_description'] = 'Blog';
			
		$this->load->helper('url');

		$data['blogs'] = $this->tomouh_model->getBlogs();

		$this->load->view('blog',$data);
	}
	public function detail($slug=''){
		$this->load->helper('url');
		
		$result = $this->tomouh_model->getBlogBySlug($slug);
		$data = $result;
		$data['meta']= $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title'] = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword'] = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['body_class'] = 'blog blog-'.$result['id'];
		
		$this->load->view('blog-detail',$data);
	}
}