<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

// require_once('./application/mailchimp/MCAPI.class.php');

include('PayPal/bootstrap.php');
use PayPal\Api\Payment;

require_once('./application/helpers/general_helper.php');
require_once('./zoho_v2/vendor/autoload.php');
//use PayPal\Api\Agreement;
		 
class Paypal_process extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->helper('url');
        $this->load->model('tomouh_model');
        $this->load->model('general_model');
        $this->load->model('messages_model');
        $this->load->model('admin/admin_model');
        $this->load->model('paging_model');
        $this->load->library('form_validation'); 
        $this->load->library('session');
        ZCRMRestClient::initialize();
    }

    public function index(){


        $data = array();

        $user_step1 = $this->session->userdata('user');

        $user_step2 = $this->session->userdata('user_step2');

        if(!isset($user_step1['email']) || $user_step1['email'] == ''){

            redirect(base_url().'signup');
            exit;
        }


        if( isset($user_step1['is_member']) ){

            $data['member_temp_id'] = $user_step1['email'];

        }else{

            $email = $user_step1['email'];

            $member_array = array(
                'v_email'=> $email,
                'l_user_step1_data'=>json_encode($user_step1),
                'l_user_step2_data'=>json_encode($user_step2),
            ); 

            $member_temp_id = $this->admin_model->add_entry($member_array,"tbl_members_temp");

            $data['member_temp_id'] = $member_temp_id;
        }


        $data['business_name'] = $this->tomouh_model->getSetting('PAYPAL_BUSINESS_NAME');
        
        $data['is_test'] = $this->tomouh_model->getSetting('IS_TEST');

        $data['charges_amount'] = $this->tomouh_model->getSetting('PAYPAL_CHARGES_AMOUNT');

        $data['meta_title'] = "Paypal_process";
        $data['meta_keyword'] = "Paypal_process";
        $data['meta_description'] = "Paypal_process";

        // echo "<pre>";
        // print_r($data);
        // exit;
        $this->load->view('paypal_process',$data);
    }

    public function isValidEmail($email){ 
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function returnPaypal(){

        $response = $_POST;

        //sleep(30);

        // $to = "harleybell.cis@gmail.com";
        // $subject = "paypal_data";

        // $from = '';

        // $message = "<pre>". print_r($_POST,true)."</pre>";
        // // $message = "hiii";

        // $this->tomouh_model->sent_email($to, $from, $subject, $message , $attachments = array() );

         $handle = fopen('./test_paypal.txt','a+');
         $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
             
         $logtext .= "========== parameter list ======== ";
            
         $logtext .= "<pre>".print_r($response, true)."</pre>";;
             
         $logtext .= "\n\n**********************************************************************\n\n";
         $errorlog = fwrite($handle,$logtext);
         fclose($handle);

        // exit();

        if(array_key_exists('payment_status',$response) && $response['payment_status']){

            $sub_id = $response['subscr_id'];

            if(isset($sub_id) && !empty($sub_id)){

                $sub_data = $this->tomouh_model->getSubscriptionBySubId($sub_id);

                $sub_date = isset($sub_data['d_subscription_date']) ? $sub_data['d_subscription_date'] : '';

                $original_date = '';

                if($sub_date != ''){

                    $original_date = date('Y-m-d', strtotime($sub_date));
                }

                if(isset($original_date) && !empty($original_date) && strtotime($original_date) >= strtotime(date('Y-m-d') .' -1 day')){

                    // exit();

                }else{

                $member = $this->tomouh_model->getUserBySubscriptionId($sub_id);

                if(!empty($member)){

                    $subscription_array = array(
                            'user_id'=>$member['id'],
                            'v_customer_id'=>$response['payer_id'],
                            'v_subscription_id'=>$sub_id,
                            'l_subscription_data'=>json_encode($response),
                            'd_subscription_date'=>date("Y-m-d H:i:s"),
                            'e_payment_type'=>'paypal',
                        );
                    $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

                    $update_member = array(
                            'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year')),
                            );

                    $this->admin_model->update_entry($member['id'],$update_member,"tbl_members");

                    $this->update_zoho_crm($member['id'], $sub_id);

                }else{

                        $is_member = $this->isValidEmail($response['custom']);

                        if( $is_member == 1 ){

                            $mem_data = $this->tomouh_model->getUserByEmail($response['custom']);

                            if($mem_data){

                                $subscription_array = array(
                                        'user_id'=>$mem_data['id'],
                                        'v_customer_id'=>$response['payer_id'],
                                        'v_subscription_id'=>$sub_id,
                                        'l_subscription_data'=>json_encode($response),
                                        'd_subscription_date'=>date("Y-m-d H:i:s"),
                                        'e_payment_type'=>'paypal',
                                    );
                                $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

                                $update_member = array(
                                        'e_plan_type' => 'paid',
                                        'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year'))
                                        );

                                $this->admin_model->update_entry($mem_data['id'],$update_member,"tbl_members");

                                $this->update_zoho_crm($mem_data['id'], $sub_id);
                            }

                        }else{

                        $member_temp_data = $this->tomouh_model->getTempUserById($response['custom']);

                        $user_data = json_decode($member_temp_data['l_user_step1_data'],true);

                        $data = json_decode($member_temp_data['l_user_step2_data'],true);
                        $latitude = ''; 
                        $longitude = ''; 
                            
                            // if(isset($user_data['residence_city'])){

                            //     $address = $user_data['residence_city'].' '.$user_data['residence_country'];   

                            //     $google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

                            //     // $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);
                                
                            //     $geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);
                                
                            //     $geo = json_decode($geo, true);

                            //     if (isset($geo['status']) && ($geo['status'] == 'OK')) {

                            //       $latitude = $geo['results'][0]['geometry']['location']['lat']; 
                            //       $longitude = $geo['results'][0]['geometry']['location']['lng']; 

                            //     }else{

                            //         $latitude = ''; 
                            //         $longitude = ''; 
                            //     }
                            // }else{
                            //         $latitude = ''; 
                            //         $longitude = ''; 
                            // }

                            $main_occupation_description = isset($data['main_occu'])?$data['main_occu']:'';

                            $main_occupation_data = isset($data['occupation'])?$data['occupation']:'';
                            
                            $int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

                            $passion_interest = $data['passions_interests'];

                            if($user_data['referral'] == 0){

                                $referral_email = '';
                            }else{

                                $referral_email = $user_data['referral_email'];
                            }

                            $user = $user_data['email'];

                            $email_to = $user;
                            $email_from = "";

                            $template = $this->tomouh_model->getEmailTemplate(2);
                            $email_subject = $template['v_subject'];
                            $content = $template['l_body'];
                            
                            $activation_link = '<a href="'.base_url().'login/activation/'.md5($user_data['email']).'" target="_blank">link here</a>';
                    
                            $content = str_replace("link here", $activation_link, $content);

                            $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

                            $tel1 = $tel2 ='';
                            if(isset($user_data['tele_country_code']) && isset($user_data['telephone'])){
                                $tel1= $user_data['tele_country_code'].'-'.$user_data['telephone'];
                            }
                            if(isset($user_data['tele_2_country_code']) && isset($user_data['telephone_2'])){
                                $tel2= $user_data['tele_2_country_code'].'-'.$user_data['telephone_2'];
                            }
                            // $sent = 1;

                            if($sent){

                                $update_data = array(
                                    'v_email'        => isset($user_data['email']) ? $user_data['email'] :'',
                                    'v_password'     => isset($user_data['password']) ? md5($user_data['password']) : '',
                                    'v_firstname'    => isset($user_data['firstname']) ? $user_data['firstname'] : '',
                                    'v_lastname'     => isset($user_data['lastname']) ? $user_data['lastname'] : '',
                                    'd_dob'          => isset($user_data['dob']) ? date('Y-m-d', strtotime($user_data['dob'])) : '',
                                    'e_gender'       => isset($user_data['gender']) ? $user_data['gender'] : '',
                                    'v_home_country' => isset($user_data['home_country']) ? $user_data['home_country'] : '',
                                    'v_home_city'    => isset($user_data['home_city']) ? $user_data['home_city'] : '',
                                    'v_residence_country' => isset($user_data['residence_country']) ? $user_data['residence_country'] : '',
                                    'v_residence_city'    => isset($user_data['residence_city']) ? $user_data['residence_city'] : '',
                                    'v_telephone'    => $tel1,
                                    'v_telephone_2'  => $tel2,
                                    'v_twitter_link '=> isset($user_data['twitter_link']) ? $user_data['twitter_link'] : '',
                                    'v_linkedin_link'=> isset($user_data['linkedin_link']) ? $user_data['linkedin_link'] : '',
                                    'v_instagram_link'=> isset($user_data['instagram_link']) ? $user_data['instagram_link'] : '',
                                    'e_referral'     => isset($user_data['referral']) ? $user_data['referral'] : '',
                                    'v_referral_email' => isset($referral_email) ? $referral_email :'',
                                    't_passion_interest'  => isset($passion_interest) ? $passion_interest : '',
                                    't_description'  => '',
                                    'v_main_occupation'=> $main_occupation_description,
                                    'v_longitude'=>$longitude,
                                    'v_latitude'=>$latitude,
                                    'e_status'=>'inactive',
                                    'e_member_of_month'=>0,
                                    'v_subscription_id'=>$sub_id,
                                    'e_type'=>isset($user_data['type']) ? $user_data['type'] : '',
									'v_label' => 'Member',
                                    'e_plan_type'=>'paid',
                                    'v_crm_contact_id'=>$user_data['contact_id'],
                                    'e_payment_type' =>'paypal',
                                    'd_subscription_exp_date'=> date('Y-m-d', strtotime('+1 year')),
                                    'd_last_profile_update' => date('Y-m-d')
                                );

                            $tbl = "tbl_members";
                            $add_user_data = $this->admin_model->update_member_entry($user_data['contact_id'],$update_data,$tbl);
                            // $add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

                            $this->add_zoho_crm($add_user_data,$response['custom']);
                            // $this->update_zoho_crm($add_user_data, $sub_id);

                            if($user_data['type'] == 'member'){

                                $this->add_mailchimp($user_data['email'],$user_data['firstname'],$user_data['lastname']);
                            }

                            $subscription_array = array(
                                    'user_id'=>$add_user_data,
                                    'v_customer_id'=>$response['payer_id'],
                                    'v_subscription_id'=>$sub_id,
                                    'l_subscription_data'=>json_encode($response),
                                    'd_subscription_date'=>date("Y-m-d H:i:s"),
                                    'e_payment_type'=>'paypal',
                                );
                            $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

                            $educ_data_length = sizeof($data['university']);

                            for($i=0;$i<$educ_data_length;$i++){
                              if(isset($data['university'][$i]) && !empty($data['university'][$i])){    
                                if($int_main_occupation <= 10){

                                    if(($i+1) == $int_main_occupation){
                                        $main_occupation = 1;                                   
                                    }else{
                                        $main_occupation = 0;
                                    } 
                                }else{
                                    $main_occupation = 0;
                                }

                                $insert_array = array(

                                    'user_id'=>$add_user_data,
                                    'v_university' =>$data['university'][$i],
                                    'v_degree'=>$data['degree'][$i],
                                    'v_major'=>$data['major'][$i],
                                    'i_passing_year'=>$data['graduation_year'][$i],
                                    'i_main_occupation'=>$main_occupation
                                );
                                $tbl="tbl_educational_data";
                                $this->admin_model->add_entry($insert_array,$tbl);
                              }
                            }

                            $company_data_length = sizeof($data['company']);

                            for($i=0;$i<$company_data_length;$i++){

                             if(isset($data['company'][$i]) && !empty($data['company'][$i])){
                                if($int_main_occupation >= 10){

                                    if(($i+11) == $int_main_occupation){
                                        $main_occupation = 1;                                   
                                    }else{
                                        $main_occupation = 0;
                                    } 
                                }else{  
                                    $main_occupation = 0;
                                }

                                $insert_array = array(

                                    'user_id'=>$add_user_data,
                                    'v_company' =>$data['company'][$i],
                                    'v_job_title'=>$data['job_title'][$i],
                                    'v_industry'=>$data['industry'][$i],
                                    'i_company_from'=>$data['company_from'][$i],
                                    'i_company_to'=>$data['company_to'][$i],
                                    'i_main_occupation'=>$main_occupation
                                );
                                $tbl="tbl_professional_data";
                                $this->admin_model->add_entry($insert_array,$tbl);
                              } 
                            }

                            $award_data_length = sizeof($data['award_details']);

                            for($i=0;$i<$award_data_length;$i++){

                               if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){
                                $insert_array = array(

                                    'user_id'=>$add_user_data,
                                    'v_achievement' =>$data['award_details'][$i],
                                );
                                $tbl="tbl_achievements_data";
                                $this->admin_model->add_entry($insert_array,$tbl);

                                // $member_update_array = array(

                                //             'user_id'=>$add_user_data,
                                //             'v_user_name'=>$user_data['firstname'].' '.$user_data['lastname'],
                                //             'l_description' =>'has Become the '.$data['award_details'][$i].'.',
                                //             'd_added'=>date("Y-m-d H:i:s"),
                                //             ); 
                                
                                // $this->admin_model->add_entry($member_update_array,"tbl_members_updates");  
                                    }      
                            }

                                $res = setcookie('user_step1', '', time() - 2592000, '/');

                                $res = setcookie('user_step2', '', time() - 2592000, '/');

                                $this->session->unset_userdata('user');
                                $this->session->unset_userdata('user_step2');                      
                            }
                        }
                    }
                }    
            }   
        }
    }

    public function add_zoho_crm($user_id,$temp_user_id){

        $member_data = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data = 'Payment Type : '.$member_data['e_payment_type'].', Subscription Id : '.$member_data['v_subscription_id'].', Activation Date : '.date('Y-m-d').', Expiration Date : '.$member_data['d_subscription_exp_date'];

        $member_temp_data = $this->tomouh_model->getTempUserById($temp_user_id);

        $user_step1 = json_decode($member_temp_data['l_user_step1_data'],true);

        $user_step2 = json_decode($member_temp_data['l_user_step2_data'],true);

        if(isset($user_step2['university']) && !empty($user_step2['university'])){

            $educ_data_length = sizeof($user_step2['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){

                $education_info[] = 'University : '.$user_step2['university'][$i].', Degree : '.$user_step2['degree'][$i].', Major : '.$user_step2['major'][$i].', Year : '.$user_step2['graduation_year'][$i];

                }
            }
        }

        if(isset($user_step2['company']) && !empty($user_step2['company'])){

            $company_data_length = sizeof($user_step2['company']);

            $experience_info = array();

            for($i=0;$i<$company_data_length;$i++){

                if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){

                $experience_info[] = 'Company : '.$user_step2['company'][$i].', Job Title : '.$user_step2['job_title'][$i].', Industry : '.$user_step2['industry'][$i].', Period : '.$user_step2['company_from'][$i].'-'.$user_step2['company_to'][$i];

                }
            }
        }

        if(isset($user_step2['award_details']) && !empty($user_step2['award_details'])){

            $award_data_length = sizeof($user_step2['award_details']);

            $award_detail = '';

            for($i=0;$i<$award_data_length;$i++){

                if(isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])){

                 $award_detail .= $user_step2['award_details'][$i].',';

                }
            }
        }

        if(isset($award_detail)){

            $AAH = rtrim($award_detail,',');

        }else{
            $AAH = '';
        }

        $educational_info = implode(' %0A ',$education_info);

        $exp_info = implode(' %0A ', $experience_info);

        $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

        $contact_id = $user_step1['contact_id'];

        $zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
        $zcrmRecordIns->setFieldValue("Last_Name",$user_step1['lastname']);
        $zcrmRecordIns->setFieldValue("Email",$user_step1['email']);
        $zcrmRecordIns->setFieldValue("First_Name",$user_step1['firstname']);
        $zcrmRecordIns->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
        $zcrmRecordIns->setFieldValue("Gender",$user_step1['gender']);
        $zcrmRecordIns->setFieldValue("Country",$user_step1['home_country']);
        $zcrmRecordIns->setFieldValue("Home_City",$user_step1['home_city']);
        $zcrmRecordIns->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
        $zcrmRecordIns->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
        $zcrmRecordIns->setFieldValue("City_Of_Residence",$user_step1['residence_city']);
        $zcrmRecordIns->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone']);
        $zcrmRecordIns->setFieldValue("Educational_Info",isset($educational_info)? urldecode($educational_info):''); 
        $zcrmRecordIns->setFieldValue("Experience_Info",isset($exp_info)? urldecode($exp_info):'');
        $zcrmRecordIns->setFieldValue("AAH",$AAH);
        $zcrmRecordIns->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
        $zcrmRecordIns->setFieldValue("Member_Status","Active");
        $zcrmRecordIns->setFieldValue("Plan","Paid");
        $zcrmRecordIns->setFieldValue("Label","Member");
        $zcrmRecordIns->setFieldValue("Profile_Expiry", date('Y-m-d',strtotime('+1 year')));
        $zcrmRecordIns->setFieldValue("Subscription_Info", $payment_data);
        $apiResponse   =$zcrmRecordIns->update();

    }

    public function update_zoho_crm($user_id, $sub_id){

        $results = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data =  $this->tomouh_model->getAllSubscriptionByUserId($user_id);

        $payment_info= [];

        if($payment_data){

            foreach($payment_data as $data){

                $payment_info[] = 'Payment Type : paypal, Subscription : '.$data['v_subscription_id'].', Expiration Date : '.date("m/d/Y", strtotime(date("Y-m-d", strtotime($data['d_subscription_date'])) . " + 1 year"));

            }
        }

        $payment_info[] = 'Payment Type : paypal, Subscription Id : '.$sub_id.', Expiration Date : '.date('m/d/Y',strtotime('+1 year'));  

        $pay_info = implode(' %0A ',$payment_info);

        $member_crm_id = $results['v_crm_contact_id'];

        $zcrmRecordMemIns = ZCRMRecord::getInstance("Contacts", $member_crm_id);
        $zcrmRecordMemIns->setFieldValue("Plan", 'Paid');
        $zcrmRecordMemIns->setFieldValue("Profile_Expiry", date('Y-m-d',strtotime('+1 year')));
        $zcrmRecordMemIns->setFieldValue("Subscription_Info", $pay_info);
        $apiResponse=$zcrmRecordMemIns->update();

        // print_r($pay_info);exit();

        // $records = $member->getRecordById()
        //             ->id($member_crm_id)
        //             ->request();         

        // $payment_data = $records[1]->data['Subscription Info'];
        
        // $payment_new_data = $payment_data.' %0A Payment Type : paypal, Subscription Id : '.$sub_id.', Expiration Date : '.date('m/d/Y',strtotime('+1 year'));
        /*
        $mResult = $member->updateRecords()
          ->addRecord(array(
                'Id' => $member_crm_id,
                'Plan' => 'Paid',
                'Expired On' => date('m/d/Y',strtotime('+1 year')),
                'Subscription Info'=>$pay_info,
          ))
          ->triggerWorkflow()
          ->request(); */
    }

    public function add_mailchimp($email,$firstname,$lastname){

        // mailchimp api call to subscribe start

        $merge_vars = array(
                'EMAIL' => $email,
                'FNAME' => $firstname,
                'LNAME' => $lastname,
            );

        mailChimpSubscribe($merge_vars);
        
        // mailchimp api call to subscribe end
    }
	
	public function repaymentPaypal(){
		
		
		 global $apiContext; 
		 
		 try {
			$params = array('count' => 20, 'start_time '=>'2018-10-31T11:00:00Z' , 'sort_by' => 'create_time', 'sort_order' => 'desc');
			$payments = Payment::all($params, $apiContext);
			echo "<pre>"; print_r($payments);
			exit;
		
		 } catch (Exception $ex) {
		
			echo "<pre>"; print_r($ex);
			exit(1);
		
		 }
		 
		 $response = file_get_contents('php://input');
		 $response = json_decode($response, true);
		  	 
		 $handle = fopen('./repaymentPaypal.txt','a+');
         $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
             
         $logtext .= "========== parameter list ======== ";
            
         $logtext .= "<pre>".print_r($response, true)."</pre>";;
         
		 $logtext .= "\n\n**********************************************************************\n\n";
         
		 
		 $errorlog = fwrite($handle,$logtext);
         fclose($handle);
	
	}
	
    public function file_get_contents_curl($url) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
?>