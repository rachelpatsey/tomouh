<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Members extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('messages_model');
		$this->load->model('tomouh_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){

		$this->load->helper('url');
		$data = array();

		$data['countries'] = $this->country();

		$data['limit'] = $limit = $this->admin_model->getSetting('RECORD_PER_PAGE');
		
		if ($this->input->get('page') && $this->input->get('page') !=''){

			$data['page'] = $this->input->get('page');
			$limitstart = $limit * ($this->input->get('page') - 1);

		}else{

			$data['page'] = 1;
			$limitstart = 0;
		}		

		if($this->input->post()){

			if($this->input->post('send_mail') == 'send_mail'){
				
				$site_email = $this->admin_model->getSetting('CONTACT_INFO');
				
				$email_subject = $this->input->post('subject');
				$content = $this->input->post('message');
				$email_to = $this->input->post('member_email');
				$user_id =  $this->session->userdata('logged_user');
				$user_data = $this->tomouh_model->getUserByUserId($user_id);
				$to_id = $this->tomouh_model->getUserByEmail($email_to);
				
				$email_from = $user_data['v_email'];
				
				$send_text = $user_data['v_firstname'].' '.$user_data['v_lastname']." sent you the message below. Please reply to them through this email.";
				
				$content = $send_text.'<br/><br/>'.$content;
				$email_subject = "Message From Tomouh Member:".$email_subject;
				
				$config = array(
				  'mailtype' => 'html',
				);
			//	$email_to = 'victoriahale.cis@gmail.com';
			//	$email_from = 'victoriahale.cis@gmail.com';
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from($email_from);
				$this->email->to($email_to);
				$this->email->cc($site_email);
				$this->email->subject($email_subject);
				$this->email->message($content);
				
				$file_name = $user_data['v_firstname'].'-'.$user_data['v_lastname'].'.pdf';
				$this->download($user_id, $file_name);


				$targetPath = getcwd() . '/uploads/pdfs/';
				// echo $targetPath.$file_name;
				// exit; 
				
				$this->email->attach($targetPath.$file_name);
				 
				$sent = $this->email->send();
				
				$ins = array(
					"i_to"	 		=> $to_id['id'],
					"i_from" 		=> $user_id,
					"v_to_email"	=> $email_to,
					"v_from_email"	=> $email_from,
					"v_subject"	 	=> $email_subject,
					"l_body"	 	=> $content,
					"d_added"	 	=> date("Y-m-d"),
				);
				$this->db->insert('tbl_communications', $ins);
				
				
				redirect(base_url().'members?succ=1&msg=send_contact_msg');
				exit();
			}
		}elseif($this->input->get('search')){

			$data['post_data'] = $this->input->get();

			$firstname = $this->input->get('firstname');
			$lastname = $this->input->get('lastname');
			$keyword = $this->input->get('keyword');
			$residence_country = $this->input->get('residence_country');
			$residence_city = $this->input->get('residence_city');
			$home_country =  $this->input->get('home_country');
			$home_city = $this->input->get('home_city');
			$university = $this->input->get('university');
			$major = $this->input->get('major');
			$degree = $this->input->get('degree');
			$industry = $this->input->get('industry');
			$company = $this->input->get('company');
			$flag = $this->input->get('advance_search_flag');

			$where = 1;
			
			if($firstname != '')
				$where .= " AND t1.v_firstname LIKE '%$firstname%'";
			
			if($lastname != '')
				$where .= " AND t1.v_lastname LIKE '%$lastname%'";
			
			if($keyword != '')
				$where .= " AND t1.v_email LIKE '%$keyword%'";
			
			if($residence_country != '')
				$where .= " AND t1.v_residence_country LIKE '%$residence_country%'";
				 
			if($residence_city != '')
				$where .= " AND t1.v_residence_city LIKE '%$residence_city%'"; 
			
			if($home_country != '')
				$where .= " AND t1.v_home_country LIKE '%$home_country%'"; 
					  
			if($home_city != '')
				$where .= " AND t1.v_home_city LIKE '%$home_city%'"; 
				
			if($university != '')
				$where .= " AND t2.v_university LIKE '%$university%'"; 
				
			if($major != '')
				$where .= " AND t2.v_major LIKE '%$major%'";
				 
			if($degree != '')
				$where .= " AND t2.v_degree LIKE '%$degree%'"; 
				
			if($industry != '')
				$where .= " AND t3.v_industry LIKE '%$industry%'"; 
				
			if($company != '')
				$where .= " AND t3.v_company LIKE '%$company%'";

			$data['members'] = $this->tomouh_model->searchMember($where,$limit,$limitstart);

			$data['flag'] = $flag;  

			$data['search_form_flag'] = 1;

		}else{

			$data['members'] = $this->tomouh_model->memberWithProfessionDataMember($limit,$limitstart);

		}

		$data['meta_keyword'] = 'Members';
		$data['meta_description'] = 'Members';
		$data['meta_title'] = 'Members';

		if($this->session->has_userdata('logged_user')){

			$this->load->view('members',$data);
			
		}else{

			redirect(base_url().'login');
			exit();
		}
	}

	public function country(){

		$country_data =$this->db->query("SELECT * FROM `tbl_country` WHERE e_status = 'active'");

		$country = $country_data->result();

		return $country;

	} 
	
	public function download($id='', $pdf_name=''){
		$this->load->helper('url');
		
		$data['id'] = $id; 
	       
		$user_data = $this->tomouh_model->getUserByUserId($id);
		$educational_data = $this->tomouh_model->getEducationalData($id);
		$professional_data = $this->tomouh_model->getProfessionalData($id);
		$achievement_data = $this->tomouh_model->getAchievementalData($id);
		
		$site_logo = $this->tomouh_model->getSetting('SITE_LOGO');
		
		//echo "<pre>"; print_r($user_data); exit;
			
		include('TCPDF/tcpdf.php');
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(5, 0, 5);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		$pdf->SetFont('helvetica', '', 3);
		$pdf->AddPage();
		ob_start();
		?><html><head></head>
		<body style="font-family: sans-serif;" >
			<div style="width: 100%; max-width: 100%; margin: 0px auto 20px; padding: 20px 0px;">
				<table style=" border-radius: 5px; width: 100%; margin: 0px auto 20px; text-align: center; background-color: #fff; float: left;">
					<tr style="margin: 15px auto;">
						<td style="width: 100%;"><img src="./assets/frontend/images/profile_logo.png" alt="Logo" width="180" height="60" /></td>
                   </tr>
					<tr style="margin: 15px auto;">
						
                    	<td style="width: 100%; padding: 0px 0px; display: table-cell; vertical-align: middle; text-align:center;" colspan="2">
                        	<h3 style="color:#800000; font-size:20px;">Member Profile - Tomouh</h3>
                            <br><br>
                        </td>
					</tr>
                    <tr style="margin: 15px auto;">
						<td style="float: left; width: 33.3333%; padding: 0px 0px; display: table-cell; vertical-align: middle;">
						<table>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr>
							<img src="./assets/frontend/images/personal_info.png" alt="Logo" height="65" width="65"/>
							<div style="font-size: 13px; margin: 0px auto 0px; letter-spacing: 0.5px;"> Personal Information </div>
							</tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
						</table>
						</td>
						<td style="float: left; width: 66%; border-left: 1px solid #ddd; padding: 50px 0px; padding-left: 50px;">
							<table style="width: 100%; text-align: left; font-size: 13px; letter-spacing: 0.5px; line-height: 25px;">
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr style="width: 100%;">
									<td style="width: 45%;">
										&nbsp; Name
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_firstname'].' '.$user_data['v_lastname']; ?>
									</td>
								</tr>
								<tr><td colspan="2">&nbsp;</td></tr>
							</table>
						</td>
					</tr>
					<tr style="margin: 15px auto;">
						<td style="float: left; width: 33.3333%;  padding: 0px; vertical-align: middle;">
							<table>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr>
							<img src="./assets/frontend/images/contact_info.png" alt="Logo" height="65" width="65"/>
							<div style="font-size: 13px; margin: 0px auto; letter-spacing: 0.5px;"> Contact Information </div>
							</tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							</table>
						</td>
						<td style="float: left; width: 60%; border-left: 1px solid #ddd; padding: 50px 0px; padding-left: 50px;">
							<table style="width: 100%; text-align: left; font-size: 13px; letter-spacing: 0.5px; line-height: 25px;">
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Home Country
									</td>
									<td style="width: 50%;"><?php echo $user_data['v_home_country']; ?>
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Home City
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_home_city']; ?>
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Country of Residence
									</td>
									<td style="width: 50%;"><?php echo $user_data['v_residence_country']; ?>
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; City of Residence
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_residence_city']; ?>
									</td>
								</tr>
								<?php /*?><tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Mobile Number
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_telephone']; ?>					
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Mobile Number 2
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_telephone_2']; ?>
									</td>
								</tr><?php */?>
							</table>
						</td>
					</tr>
					<tr style="margin-bottom: 15px;">
						<td style="float: left; width: 33.3333%; padding: 50px 0px; vertical-align: middle;">
						<table>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr>
                            <td>
							<img src="./assets/frontend/images/social.png" alt="Logo" height="65" width="65"/>
							<div style="font-size: 13px; margin: 10px auto; letter-spacing: 0.5px;"> Social </div>
							</td>
                            </tr>

							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							<tr><td >&nbsp;</td></tr>
							</table>
						</td>
						<td style="float: left; width: 60%; border-left: 1px solid #ddd; padding: 50px 0px; padding-left: 50px;">
							<table style="width: 100%; text-align: left; font-size: 13px; letter-spacing: 0.5px; line-height: 25px;">
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Twitter Link
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_twitter_link']; ?>
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; LinkedIn Link
									</td>
									<td style="width: 50%;">
										<?php echo $user_data['v_linkedin_link']; ?>
									</td>
								</tr>
								<tr style="width: 100%;">
									<td style="width: 50%;">
										&nbsp; Instagram Link
									</td>
									<td style="width: 50%;"><?php echo $user_data['v_instagram_link']; ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/><br/>
				<table style="border: 1px solid #ddd;  border-radius: 5px; width: 100%; margin: 0px auto 0px; text-align: center; background-color: #fff; float: left; border-collapse: collapse;">
					<tr><td >&nbsp;</td></tr>
					<tr><td >&nbsp;</td></tr>
					<tr style="margin-bottom: 15px;">
						<td style="float: left; width:100%; padding: 25px 0px 30px; vertical-align: middle;">
							<div style="padding: 0px 15px; letter-spacing: 0.5px; font-size: 15px; font-weight: 600; margin: 0px; text-align: left;"> <b> Educational Information </b> 
							</div>
						</td>
					</tr>
					<tr><td >&nbsp;</td></tr>
					<tr><td >&nbsp;</td></tr>
					<tr><td >&nbsp;</td></tr>
					<tr><td >&nbsp;</td></tr>
					<tr>
			        	<td style="float:left;width:100%;padding:25px 0px 30px;vertical-align: middle;">
						<table style="background-color: #fff;font-size:13px;width:100%;text-align:left;border-collapse:collapse; padding-bottom:25px;">
							<thead>
							<tr style="border-bottom: 2px solid #ccc;">
								<th style="float:left;width:32.5%;line-height:20px;">UNIVERSITY</th>
								<th style="float:left;width:20%;line-height:20px;">DEGREE</th>
								<th style="float:left;width:20%;line-height:20px;">MAJOR </th>
								<th style="float:left;width:20%;line-height:20px;">YEAR</th>
							</tr>
							</thead>
							<tbody>
                            	<?php foreach($educational_data as $row){ 
									if($row['i_passing_year'] == 'current'){
										$row['i_passing_year'] = 'present';
									}
								?>
								<tr>
									<td style="float:left;width:32.5%;padding:5px 0px 5px;line-height:10px;line-height:15px;"><?php echo $row['v_university']; ?></td>
									<td style="float:left;width:20%;padding:5px 0px 5px;line-height:10px;line-height:15px;"><?php echo $row['v_degree']; ?></td>
									<td style="float:left;width:20%;padding:5px 0px 5px;line-height:10px;line-height:15px;"><?php echo $row['v_major']; ?></td>
									<td style="float:left;width:20%;padding:5px 0px 5px;line-height:10px;line-height:15px;"><?php echo $row['i_passing_year']; ?></td>
								</tr>
                                <?php }?>
							</tbody>
						</table>
			            </td>
					</tr>
				</table>
				
				</div>
		</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

ob_start();
?>		
<html><head></head>
<body style="font-family: sans-serif;" >
    <div style="width: 100%; max-width: 100%; margin: 10px auto 20px; padding: 20px 0px;">
<br/><br/><br/><br/>
<table style="border: 1px solid #ddd;  border-radius: 5px; width: 100%; margin: 0px auto 0px; text-align: center; background-color: #fff; float: left; border-collapse: collapse;">
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr style="margin-bottom: 15px;">
                <td style="float: left; width:100%; padding: 25px 0px 30px; vertical-align: middle;">
                    <div style="padding: 0px 15px; letter-spacing: 0.5px; font-size: 15px; font-weight: 600; margin: 0px; text-align: left;"> <b> Professional Information </b> 
                    </div>
                </td>
            </tr>
            <tr><td >&nbsp;</td></tr>
            <tr><td >&nbsp;</td></tr>
            <tr><td >&nbsp;</td></tr>
            <tr><td >&nbsp;</td></tr>
            <tr>
                <td style="float: left; width: 100%; padding: 25px 0px 30px; vertical-align: middle;">
                <table style="background-color: #fff; width: 100%;  font-size: 13px; text-align: left; border-collapse: collapse; padding-bottom: 25px;">
                    <thead>
                    <tr style="border-bottom: 2px solid #ccc;">
                        <th style="float:left;width:20%;padding:5px 0px;padding-left:15px;line-height:20px;">COMPANY</th>
                        <th style="float:left;width:22%;padding:5px 0px;padding-left:15px;line-height:20px;">JOB TITLE</th>
                        <th style="float:left;width:20%;padding:5px 0px;padding-left:15px;line-height:20px;">INDUSTRY </th>
                        <th style="float:left;width:18%;padding:5px 0px;padding-left:15px;line-height:20px;">FROM</th>
                        <th style="float:left;width:18%;padding:5px 0px;padding-left:15px;line-height:20px;">TO</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($professional_data as $row){ 
								if($row['i_company_to'] == 'current'){
									$row['i_company_to'] = 'present';
								}
						?>
                        <tr>
                            <td style="float:left;width:20%;padding:10px 0px 10px;margin-left:15px;line-height:10px;line-height:15px;"><?php echo $row['v_company']; ?></td>
                            <td style="float:left;width:22%;padding:10px 0px 10px;margin-left:15px;line-height:10px;line-height:15px;"><?php echo $row['v_job_title']; ?></td>
                            <td style="float:left;width:20%;padding:10px 0px 10px;margin-left:15px;line-height:10px;line-height:15px;"><?php echo $row['v_industry']; ?></td>
                            <td style="float:left;width:18%;padding:10px 0px 10px;margin-left:15px;line-height:10px;line-height:15px;"><?php echo $row['i_company_from']; ?></td>
                            <td style="float:left;width:18%;padding: 10px 0px 10px;margin-left:15px;line-height:10px;line-height:15px;"><?php echo $row['i_company_to']; ?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                </td>
            </tr>
        </table>
<br/><br/><br/><br/><br/>
<table style="border:1px solid #ddd;border-radius:5px;width:100%;max-width:950px;margin:20px auto 0px;text-align:center; background-color:#fff;float:left;border-collapse:collapse;">
    <tr><td >&nbsp;</td></tr>
    <tr><td >&nbsp;</td></tr>
    <tr style="margin-bottom: 15px;">
        <td style="float:left;width:100%;vertical-align:middle;">
            <div style="padding:0px 15px;letter-spacing:0.5px;font-size:15px;margin:0px;text-align:left;"> <b> Achievements, Awards and Honors </b> </div>
        </td>
    </tr>
    <tr><td >&nbsp;</td></tr>
    <tr><td >&nbsp;</td></tr>
    <tr style="text-align: left;">
        <td style="font-size: 13px;">
            <ul>
            <?php foreach($achievement_data as $row){ ?>

                <li><?php echo $row['v_achievement']; ?></li>

            <?php } ?>
            </ul>
            
        </td>
    </tr>
</table>
<br/><br/><br/><br/><br/>	
<table style="border: 1px solid #ddd; border-radius: 5px; width: 100%; margin: 20px auto 0px; text-align: center; background-color: #fff; float: left; border-collapse: collapse;">
    <tr><td >&nbsp;</td></tr>
    <tr><td >&nbsp;</td></tr>
    <tr style="margin-bottom: 15px;">
        <td style="float: left; width: 100%; padding: 15px 0px; vertical-align: middle;">
            <div style="padding: 0px 15px; letter-spacing: 0.5px; font-size: 15px; font-weight: 600; margin: 0px; text-align: left;"> <b> Passions and Interests </b> </div>
        </td>
    </tr>
    <tr>
        <td style="float: left; width: 100%; padding: 15px 0px; vertical-align: middle;">
        <div style="padding: 0px 15px; letter-spacing: 0.5px; font-size: 13px; font-weight: 100; margin: 0px; text-align: left;"><?php echo $user_data['t_passion_interest']; ?>&nbsp;&nbsp;&nbsp;</div></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
</table>
</div>
</body>
</html>
<?php
	    $html2 = ob_get_contents();
   		ob_end_clean();
   		
			
		$pdf->writeHTML($html, true, 0, true, 0);
		$pdf->AddPage();
		$pdf->writeHTML($html2, true, 0, true, 0);
		
		$pdf->lastPage();
		if($pdf_name != ''){
			$targetPath = getcwd() . '/uploads/pdfs/';
			$pdf->Output($targetPath.$pdf_name, 'F');
		}else{
			$pdf->Output($user_data['v_firstname'].' '.$user_data['v_lastname'].'.pdf', 'I');
		}
	}
}