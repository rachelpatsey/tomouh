<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once('./zoho_v2/vendor/autoload.php');

class AddLiveMembers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('admin/admin_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');

		ZCRMRestClient::initialize();
	}

	public function index()
	{

		// bilal : 4026017000000217013
		// testuser2 : 4026017000000217103

		// $moduleIns = ZCRMRestClient::getInstance()->getModuleInstance("Contacts");  //To get module instance
		// 		// $response = $moduleIns->searchRecordsByCriteria("(Email:equals:$email)");  //To get module records that match the criteria
		// 		$memberData=$moduleIns->getRecords(); 

		$this->db->select('*');
		$this->db->from('tbl_members_live');
		// $this->db->where('id', 45);
		$this->db->order_by('id', 'ASC')->limit(1);
		$query = $this->db->get();
		$row = $query->result_array();
		// echo "<pre>";
		// print_r($row);
		// exit;
		foreach ($row as $key => $value) {
			$user_id = $value['id'];
			// echo $user_id; 
			// achivement data
			$this->db->select('*');
			$this->db->from('tbl_achievements_data');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$rowa = $query->result_array();
			$achvdata = [];
			$achv = '';
			if (count($rowa)) {
				foreach ($rowa as $ka => $va) {
					$achvdata[] = $va['v_achievement'];
				}
				$achv = implode(',', $achvdata);
			}


			// educational information
			$this->db->select('*');
			$this->db->from('tbl_educational_data');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$rowe = $query->result_array();
			$education_info = [];
			$educational = '';
			if (count($rowe)) {
				foreach ($rowe as $ke => $ve) {
					$education_info[] = 'University : ' . $ve['v_university'] . ', Degree : ' . $ve['v_degree'] . ', Major : ' . $ve['v_major'] . ', Year : ' . $ve['i_passing_year'];
				}
				$educational = implode(' %0A ', $education_info);
				$educational = urldecode($educational);
			}

			// professional information
			$this->db->select('*');
			$this->db->from('tbl_professional_data');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$rowp = $query->result_array();
			$experience_info = [];
			$experience = '';
			if (count($rowp)) {
				foreach ($rowp as $kex => $vex) {
					$experience_info[] = 'Company : ' . $vex['v_company'] . ', Job Title : ' . $vex['v_job_title'] . ', Industry : ' . $vex['v_industry'] . ', Period : ' . $vex['i_company_from'] . '-' . $vex['i_company_to'];
				}
				$experience = implode(' %0A ', $experience_info);
				$experience = urldecode($experience);
			}

			// subscription information
			$this->db->select('*');
			$this->db->from('tbl_subscription_data');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$rowsub = $query->result_array();
			$payment_info = [];
			$payement = '';
			if (count($rowsub)) {
				foreach ($rowsub as $kex => $vex) {
					$payment_info[] = 'Payment Type : ' . $vex['e_payment_type'] . ', Subscription : ' . $vex['v_subscription_id'] . ', Expiration Date : ' . date('m/d/Y', strtotime($vex['d_subscription_date']));
				}
				$payement = implode(' %0A ', $payment_info);
				$payement = urldecode($payement);
			}

			// insert contact in zoho start

			// to get the instance of the module

			$moduleContactIns = ZCRMRestClient::getInstance()->getModuleInstance("Contacts");
			$recordsContact = array();

			//To get ZCRMRecord instance
			$recordContact = ZCRMRecord::getInstance("Contacts", null);

			//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
			$recordContact->setFieldValue("Last_Name", isset($value['v_lastname']) ? $value['v_lastname'] : '');
			$recordContact->setFieldValue("Email", isset($value['v_email']) ? $value['v_email'] : '');
			$recordContact->setFieldValue("First_Name", isset($value['v_firstname']) ? $value['v_firstname'] : '');
			$recordContact->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($value['d_dob'])));
			$recordContact->setFieldValue("Gender", $value['e_gender']);

			// $recordContact->setFieldValue("Profile_Expiry","10/10/2020");
			$recordContact->setFieldValue("Home_Country", isset($value['v_home_country']) ? $value['v_home_country'] : '');
			$recordContact->setFieldValue("Home_City", isset($value['v_home_city']) ? $value['v_home_city'] : '');
			$recordContact->setFieldValue("Mobile", isset($value['v_telephone']) && !empty($value['v_telephone']) ?  $value['v_telephone'] : '');
			$recordContact->setFieldValue("Mobile_No", isset($value['v_telephone']) && !empty($value['v_telephone']) ? $value['v_telephone'] : '');

			$recordContact->setFieldValue("Country_Of_Residence", isset($value['v_residence_country']) ? $value['v_residence_country'] : '');
			$recordContact->setFieldValue("City_Of_Residence", isset($value['v_residence_city']) ? $value['v_residence_city'] : '');
			$recordContact->setFieldValue("Mobile_No_2", isset($value['v_telephone_2']) && !empty($value['v_telephone_2']) ? $value['v_telephone_2'] : '');
			$recordContact->setFieldValue("Educational_Info", $educational);

			$recordContact->setFieldValue("Experience_Info", $experience);
			$recordContact->setFieldValue("AAH", $achv);
			$recordContact->setFieldValue("Passions_and_interests", isset($value['t_passion_interest']) ? $value['t_passion_interest'] : '');
			$recordContact->setFieldValue("Subscription_Info", $payement);

			$recordContact->setFieldValue("Lead_Source", isset($value['lead_source']) ? $value['lead_source'] : '');
			$recordContact->setFieldValue("Label", $value['v_label']);
			$recordContact->setFieldValue("Plan", $value['e_plan_type']);
			$recordContact->setFieldValue("Lead_Source", $value['v_lead_source']);

			$recordContact->setFieldValue("GCC_or_Not", $value['v_gcc']);
			$recordContact->setFieldValue("Rank", $value['v_rank']);

			$recordContact->setFieldValue("Last_ranked", $value['last_ranked']);
			$recordContact->setFieldValue("Industry", $value['last_industry']);
			$recordContact->setFieldValue("Expired_On", $value['profile_expiry']);
			$recordContact->setFieldValue("Hosted_Events", $value['hosted_event']);
			$recordContact->setFieldValue("Spoke_at_event", $value['spoke_at_event']);
			$recordContact->setFieldValue("Moderator", $value['moderator']);

			$recordContact->setFieldValue("Membership_Committee", $value['membership_committee']);
			$recordContact->setFieldValue("Age", $value['age']);
			$recordContact->setFieldValue("Last_profile_update", $value['last_profile_update']);
			$recordContact->setFieldValue("NewTeam", $value['new_team']);
			$recordContact->setFieldValue("Ambassador", $value['ambassador']);
			$recordContact->setFieldValue("Flagged", $value['flagged']);
			$recordContact->setFieldValue("Last_Modified", $value['last_modified']);
			$recordContact->setFieldValue("Secondary_Email", $value['secondary_email']);


			$value['v_owner_id'] = 4026017000000217103;
			$user = ZCRMUser::getInstance($value['v_owner_id'], null);
			$recordContact->setOwner($user);

			array_push($recordsContact, $recordContact); //pushing the record to the array 

			$responseIn = $moduleContactIns->createRecords($recordsContact);
			$responseIn = $responseIn->getEntityResponses();
			$responseIn = $responseIn[0]->getDetails();
			$responseCrmID = $responseIn['id'];

			$update_array  = array(
				'v_crm_contact_id' => $responseCrmID,
			);
			// echo $user_id; exit;
			$this->admin_model->update_entry($user_id, $update_array, "tbl_members_live");
		}

		echo "<pre>";
		print_r('added');
		exit;
	}
}
