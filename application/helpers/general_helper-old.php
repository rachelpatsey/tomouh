<?php 
	use \DrewM\MailChimp\MailChimp;
	
	function _p($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}

	function mailChimpSubscribe($merge_vars){

		include("./application/mailchimp/MailChimp.php");
		

		$CI = get_instance();
		$CI->load->model('tomouh_model');

		$list_id = $CI->tomouh_model->getSetting('MAILCHIMP_LIST_ID');
		$api_key = $CI->tomouh_model->getSetting('MAILCHIMP_API_KEY');
		$emailHash = md5($merge_vars['EMAIL']);

		$MailChimp = new MailChimp($api_key);

		try{					
			$result = $MailChimp->put("lists/".$list_id."/members/".$emailHash, array(
				"email_address" => $merge_vars['EMAIL'],
				"merge_fields" => $merge_vars,
				"status"	   => 'subscribed',
			));
			
			if($result['status'] != 400){
				// echo "Subscribed !!!";		
			}else {

			
				 // echo $result['detail'];
			}
			
		}catch(Exception $e) {
			// echo $e;
		}
	}

	function mailChimpUnsubscribe($merge_vars){

		include("./application/mailchimp/MailChimp.php");
		
		$CI = get_instance();
		$CI->load->model('tomouh_model');

		$list_id = $CI->tomouh_model->getSetting('MAILCHIMP_LIST_ID');
		$api_key = $CI->tomouh_model->getSetting('MAILCHIMP_API_KEY');

		$emailHash = md5($merge_vars['EMAIL']);

		$MailChimp = new MailChimp($api_key);

		try{					
			$result = $MailChimp->put('/lists/'.$list_id.'/members/'.$emailHash, array(
				"email_address" => $merge_vars['EMAIL'],
				"merge_fields" => $merge_vars,
				"status"	   => 'unsubscribed',
			));
		

		if($result['status'] != 400){
			// echo "Subscribed !!!";		
		}else {
			 // echo $result['detail'];
		}
			
		}catch(Exception $e) {
			// echo $e;
		}
		
	}

	function mailChimpDelete($Email){

		include("./application/mailchimp/MailChimp.php");
		
		$CI = get_instance();
		$CI->load->model('tomouh_model');

		$list_id = $CI->tomouh_model->getSetting('MAILCHIMP_LIST_ID');
		$api_key = $CI->tomouh_model->getSetting('MAILCHIMP_API_KEY');

		$MailChimp = new MailChimp($api_key);

		$subscriber_hash = $MailChimp->subscriberHash($Email);

		$res = $MailChimp->delete("lists/$list_id/members/$subscriber_hash");
	
		return $res;
		
	}

	function mailChimpChangeEmail($Email,$oldemail){

		include("./application/mailchimp/MailChimp.php");
		
		$CI = get_instance();
		$CI->load->model('tomouh_model');

		$list_id = $CI->tomouh_model->getSetting('MAILCHIMP_LIST_ID');
		$api_key = $CI->tomouh_model->getSetting('MAILCHIMP_API_KEY');

	
		$emailHash = md5($oldemail);
		$MailChimp = new MailChimp($api_key);


		$result = $MailChimp->put('/lists/'.$list_id.'/members/'.$emailHash, 
		    array(
		        'email_address' => $Email,
		        'merge_fields' => array("EMAIL" => $Email),
		        'status' => "subscribed",
		    ));

		return $res;
		
	}
?>