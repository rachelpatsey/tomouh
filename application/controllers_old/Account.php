<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/helpers/general_helper.php');
// require_once('./zoho/vendor/autoload.php');
// use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

class Account extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('admin/admin_model');
		$this->load->model('messages_model');
		$this->load->model('tomouh_model');
		$this->load->library('session');
		$this->load->library('form_validation');

		ZCRMRestClient::initialize();
	}
	public function index(){

		if($this->session->has_userdata('logged_user')){

			$user_id = $this->session->userdata('logged_user');

			$data = array();

			$user_data = $this->tomouh_model->getUserByUserId($user_id);
	       
			$data['email'] = $user_data['v_email'];

			$password = $user_data['v_password'];

			if($this->input->post()){

				$email = $this->input->post('email');

				if($email != $user_data['v_email']){

					mailChimpChangeEmail($email,$user_data['v_email']);
				}

				$change_email = $this->input->post('change_email');

				if(isset($change_email) && !empty($change_email)){

				if(isset($email) && !empty($email)){

					// $user_data = $this->tomouh_model->getUserByUserId($user_id);

					$results = $this->tomouh_model->getAllMembers();

					if($email != $user_data['v_email']){

					$flag = 0;	

				    foreach($results as $result){

				      $saved_email = $result['v_email'];

				      if($email == $saved_email){

				      		redirect(base_url().'account/?succ=0&msg=sameemail');
							exit;
				      }

				      $flag = 1;
				    }

				    if($flag == 1){

				    	$update_array = array(
				    		'v_email'=>$email,
				    	);

				    	$this->admin_model->update_entry($user_id,$update_array,"tbl_members");
				    	$this->update_zoho_crm_email( $email, $user_id );
				    	redirect(base_url().'account/?succ=1&msg=emailupdated');
						exit;
				      }
				    }
				  }

				  

				  redirect(base_url().'account');
				  exit;
				}

				$old_password = $this->input->post('old_password');
				$new_password = $this->input->post('new_password');
				$confirm_password = $this->input->post('confirm_password');

				if($new_password != $confirm_password){

					redirect(base_url().'account/?succ=0&msg=mispass');
					exit;
				}

				if(md5($old_password) != $password){

					redirect(base_url().'account/?succ=0&msg=password_not_match');
					exit;
				}

				if(empty($new_password) || empty($confirm_password) || empty($old_password) ){

					redirect(base_url().'account/?succ=0&msg=password_blank');
					exit;
				}

				$update_array = array(
						'v_password' => md5($new_password),
						);

				$tbl = "tbl_members";

				$this->admin_model->update_entry($user_id,$update_array,$tbl);

				redirect(base_url().'account/?succ=1&msg=password_change_success');
				exit;

			}

			$data['contact_info'] =  $this->tomouh_model->getSetting('CONTACT_INFO');
			$data['meta_keyword'] = 'Account';
			$data['meta_description'] = 'Account';
			$data['meta_title'] = 'Account';

			$this->load->helper('url');
			$this->load->view('account',$data);

		}else{

			redirect(base_url().'login');
			exit();
		}
	}

	public function update_zoho_crm_email( $email, $user_id ){
		// echo $email; exit;
		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		$cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){
		
			$zcrmRecordIns = ZCRMRecord::getInstance("Members",$user_data['v_member_crm_id']);
		// 	echo "<pre>";
		// print_r($zcrmRecordIns);exit;

			$zcrmRecordIns->setFieldValue("Email", $email);
			$apiResponse=$zcrmRecordIns->update();
		
		}

		if(isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])){
		
			$zcrmRecordIns = ZCRMRecord::getInstance("Contacts",$user_data['v_crm_contact_id']);
			$zcrmRecordIns->setFieldValue("Email", $email);
			$apiResponse=$zcrmRecordIns->update();
		
		}
		/*
        $member = new ZohoCRMClient('CustomModule1', $cms_token,"eu");
        $mResult = $member->updateRecords()
            ->addRecord(
                array(
                	'Id'	 	=> isset($user_data['v_member_crm_id']) ? $user_data['v_member_crm_id'] : '',
									'Email' => $email,
                )
            )
            ->triggerWorkflow()
            ->request();  

        $client = new ZohoCRMClient('Contacts', $cms_token,"eu");

        if(isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])){

	        $contact_id = $user_data['v_crm_contact_id'];

	        $records = $client->updateRecords()
	                          ->addRecord(array(
	                                'Id' 		=> $contact_id,
				                    			'Email' => $email,
	                          ))
	                          ->triggerWorkflow()
	                          ->request();
        }
        */

	}
}
?>