<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho_v2/vendor/autoload.php');

class Crm_contact_data extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		ZCRMRestClient::initialize();
	}

	public function index(){

		$tbl = "tbl_crm_contact_data";

		$data = $_POST;

		$contact_id = $data['Contact_Id'];

		if(!empty($contact_id)){

			$crm_data =  $this->tomouh_model->getCrmContactDataById($contact_id);

			if(empty($crm_data)){

				$insert_array = array(
					'v_contact_id'=>$data['Contact_Id'],
					'v_email'=>$data['Email'],
					'l_contact_data'=>json_encode($data),
					'd_added'=>date('Y-m-d H:i:s'),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$add_contact_data = $this->admin_model->add_entry($insert_array,$tbl);	

			}else{

				$insert_array = array(
					'v_contact_id'=>$data['Contact_Id'],
					'v_email'=>$data['Email'],
					'l_contact_data'=>json_encode($data),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$this->admin_model->update_crm_contact_entry($contact_id,$insert_array,$tbl);
			}
		}

		// exit();
	}

	public function delete_crm_contact(){

		$data = $_POST;

		$contact_data = $this->tomouh_model->getCrmContactDataById($data['Contact_id']);

		$this->admin_model->delete_crm_contact($data['Contact_id'],"tbl_crm_contact_data");

		if(isset( $contact_data['v_email'] ) && !empty( $contact_data['v_email'] )){

			$this->admin_model->delete_member_temp_data( $contact_data['v_email'] , 'tbl_members_temp');

			$this->admin_model->delete_member_before_data( $contact_data['v_email'] , 'tbl_before_member');
		}
	}


	public function delete_zoho_crm_member( $member_id ){

		if(!empty($member_id)){

			$zcrmRecordIns = ZCRMRecord::getInstance("Members", $member_id); 
			$apiResponse=$zcrmRecordIns->delete();

		}
		
	}
}
?>