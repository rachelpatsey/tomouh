<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller

{

	public function __construct(){

		parent::__construct();



		$this->load->model('tomouh_model');

		$this->load->model('messages_model');

		$this->load->library('session');

		$this->load->library('form_validation');

	}

	public function index($slug=''){



		$this->load->helper('url');



		$data = array();
		
		if($slug){

			$category = $this->tomouh_model->getCategoryPhoto(urldecode($slug));

			$data['category_name'] = $category['v_name'];
			$data['category_desc'] = $category['l_description'];

			$id = $category['id'];

			$data['meta_title'] = ($category['v_meta_title']) ? $category['v_meta_title'] : $category['v_name'];

			$data['meta_keyword'] = $category['v_meta_keyword'];
			$data['meta_description'] = $category['l_meta_description'];
			$photo = $this->tomouh_model->getChildPhotos($id);
			$childs= array();
			foreach($photo as $ph){
				$photo_id = $ph['id'];
				$category_name = $ph['v_name'];
				$image = $this->tomouh_model->getPhoto($photo_id);
				$img = $image[0]['v_image'];
				$childs[] = array(
					'id' => $photo_id,
					'v_slug' => $ph['v_slug'],
					'v_name' => $category_name,
					'v_image' =>$img,
				);
			}
			$data['childs'] = $childs;
			$data['photos'] = $this->tomouh_model->getPhoto($id);

		}



		$this->load->view('category',$data);

	}

}