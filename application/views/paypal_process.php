<?php
include('inc/header_top.php');
include('inc/header.php');
?>
<div style="margin-top:500px;">
	
</div>
<style type="text/css">
	.loader {
     position: absolute;
     left: 50%;
     top: 50%;
     z-index: 1;
     width: 150px;
     height: 150px;
     margin: -75px 0 0 -75px;
     border: 16px solid #f3f3f3;
     border-radius: 50%;
     border-top: 16px solid #e04646;
     width: 120px;
     height: 120px;
     background-color: rgba(0, 0, 0, 0.5);
     -webkit-animation: spin 2s linear infinite;
     animation: spin 2s linear infinite;
   }
   #loader {
      background-color: rgba(0, 0, 0, 0.5);
     position: fixed;
      top: 0;
      right: 0;
      left: 0;
      bottom: 0;
      z-index: 99999;
   }

   @-webkit-keyframes spin {
     0% { -webkit-transform: rotate(0deg); }
     100% { -webkit-transform: rotate(360deg); }
   }

   @keyframes spin {
     0% { transform: rotate(0deg); }
     100% { transform: rotate(360deg); }
   }

</style>

<div id="loader">
   <div class="loader"></div>
</div>
<?php echo $is_test;  ?>
 <form action="<?php if($is_test == '1'){ ?>https://www.sandbox.paypal.com/cgi-bin/webscr<?php }else{ ?>https://www.paypal.com/cgi-bin/webscr<?php } ?>" id="paypal_form" name="paypal_form" method="post">
    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="<?php echo $business_name; ?>">
    <!-- Specify a Subscribe button. -->
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <!-- Identify the subscription. -->
    <input type="hidden" name="item_name" value="Tomouh Subscription Plan">
    <input type="hidden" name="rm" value="2">

    <!-- Set the terms of the regular subscription. -->
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="a3" value="<?php echo $charges_amount; ?>">
    <input type="hidden" name="p3" value="1">
    <input type="hidden" name="t3" value="Y">
    <input type="hidden" name="custom" value="<?php echo $member_temp_id; ?>">

    <input type='hidden' name='cancel_return' value='<?php echo base_url(); ?>payment'>
    <input type='hidden' name='return' value='<?php echo base_url(); ?>payment_success'>
    <input type='hidden' name='notify_url' value='<?php echo base_url(); ?>paypal_process/returnPaypal'>

    <!-- Set recurring payments until canceled. -->
    <input type="hidden" name="src" value="1">
</form>

<script type="text/javascript">
	 $( document ).ready(function() {
         $( "#paypal_form" ).submit();
     });
</script>
<?php include('inc/footer.php'); ?>