<?php 
include("inc/header_top.php");
include("inc/header.php"); ?>
<div class="after-login">
    <style type="text/css">
      .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
      .error_box .icon{float: left;padding-right: 10px;}
      input.parsley-error {
        border: 1px solid red;
      }
      .parsley-error-list {
          color: red;
      }
    </style>
        
<div class="inner_wrapper">
            <?php 
              if(!($this->form_validation->error_array())){ 
    
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>

                <div id="response" class="error_box">
                 <?php  if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
                </div> 
              <?php  } 
               }?>
              <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
  <div class="inner_header_div">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8 col-xs-12">
          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>
        </div>
        <div class="col-sm-5 col-md-4  col-xs-12">
          <div class="right-div"> <a href="<?php echo base_url(); ?>account" class="active">Account</a> <a href="<?php echo base_url(); ?>profile">Profile</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="account_page">
    <section class="event_cal">
      <div class="container"> 
           <!--  <div class="alert alert-danger notification-alert-dashboard">
                <i class="fa fa-warning sign" aria-hidden="true"></i>
                <strong></strong> 
            </div> -->
            <!-- <div class="alert alert-warning">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-warning sign"></i><strong>Alert!</strong> If You Wish to Cancel Your Membership, Please Email us at <a href="mailto:<?php echo $contact_info; ?>"><?php echo $contact_info; ?></a>
            </div> -->
      </div>
      <div class="account_custom">
        <div class="container" style="margin-bottom:20px;"> 
          <div class="page_head">
            <h1 class="red-text text-center font36">Account Information</h1>
          </div>
          <div class="border-box">
            <div class="email-block">
              <form action="" method="post" parsley-validate>
              <div class="row">
                <div class="col-md-2 col-xs-12">
                  <label>Email Address</label>
                </div>
                <div class="col-md-8 col-xs-12">
                  <input type="email" name="email" class="form-control act_inptbx" parsley-required="true" parsley-type="email" value="<?php echo $email ?>">
                </div>
                <div class="col-md-2 col-xs-12">
                  <button class="btn red-btn red-btnact" type="submit" name="change_email" value="change email">Update</button>
                </div>
              </div>
             </form>
            </div>
            <form action="" method="post" class="change_password" parsley-validate>
              <div class="row">
                <div class="col-xs-12">
                  <p class="form-head">Change Password</p>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>Old Password</label>
                    <input type="password" name="old_password" class="form-control" parsley-required="true" parsley-minlength="6" parsley-maxlength="12">
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>New Password</label>
                    <input type="password" name="new_password" class="form-control" parsley-required="true" parsley-minlength="6" parsley-maxlength="12">
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" parsley-required="true" parsley-minlength="6" parsley-maxlength="12">
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <button class="btn red-btn red-btnact2" type="submit">Submit</button>
                </div>
              </div>
            </form>
            <div class="col-sm-12" style="margin-top: 10px;margin-left: -15px;">
             <p>If you wish to cancel your membership, write us at <a href="mailto:<?php echo $contact_info; ?>"><?php echo $contact_info; ?></a></p>
             <p style="margin-top: 10px;">If you want to delete your account on this website, click the delete button <span data-toggle="modal" data-target="#myModal" id="delete_account" style="cursor: pointer; color: #8a2525;"><button class="btn red-btn">Delete</button></span></p>
             <p style="margin-top: 10px;">If you want to know what information related to you are on the website, click the request data button <a href="<?php echo base_url(); ?>request_website_data" style="cursor: pointer; color: #8a2525;"><button class="btn red-btn">Request Data</button></span></p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" style="font-weight: 900;text-align: center;">Are you sure you want to delete account!!</h4>
      <div style="margin-top: 10px;" class="text-center">  
        <button type="button" class="btn btn-danger"><a href="<?php echo base_url(); ?>delete_account" style="color:white;">Confirm</a></button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<?php include('inc/footer.php'); ?>