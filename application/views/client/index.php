<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Tomouh</title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/client/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/client/css/webapplinks.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.carousel.css">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/client/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/font-awesome.min.css">
</head>

<body>
<div class="main-header">
  <nav class="navbar navbar-fixed-top hidden-xs hidden-sm" id="menu">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/client/images/logo@2x.png" alt="" class="img-responsive"></a> </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="index.html">Home</a></li>
          <li class="dropdown"><a class="dropdown-toggle"  data-toggle="dropdown" href="#">About Us</a>
            <ul class="dropdown-menu">
              <li class=""><a href="about_us.html">About Tomouh</a></li>
              <li class=""><a href="team.html">Team</a></li>
              <li><a href="honory_member.html">Honorary Members</a></li>
            </ul>
          </li>
          
          <!--<li><a href="why_join.html">Why Join</a></li>-->
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Membership</a>
            <ul class="dropdown-menu">
              <li><a href="membership.html">Membership Process</a></li>
              <li><a href="team_committee.html">Membership Committee</a></li>
              <li><a href="signup.html">Apply to Tomouh</a></li>
              <li><a href="faq.html">FAQs</a></li>
            </ul>
          </li>
          <li class="dropdown "><a class="dropdown-toggle" data-toggle="dropdown" href="#">The Latest</a>
            <ul class="dropdown-menu">
             <li class=""><a href="media.html">Media</a></li>
              <li><a href="blog.html">Blog</a></li>
            </ul>
          </li>
          <li class=""><a href="event.html">Events</a></li>
          <li class=""><a href="contact_us.html">Contact</a></li>
          <li class="login_signup"><a href="signup" class="signup">Sign Up</a><a href="login.html" class="login">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
<div class="mobile_responsive_menu hidden-lg hidden-md">
    <div class="mobile_logo" id="menu1"> <a href="index.html"><img src="<?php echo base_url(); ?>assets/client/images/logo@2x.png" alt="" class="img-responsive"></a> </div>
    <header>
      <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>
        <div class="wrapper clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> </div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list clearfix">
            <li class="active"><a href="index.html">Home<span class="wsmenu-click"><i class="fa fa-close" onClick="hideMenu()"></i></span></a></li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">About Us<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li class=""><a href="about_us.html">About Tomouh</a></li>
                <li><a href="team.html">Team</a></li>
                <li class=""><a href="honory_member.html">Honorary Members</a></li>
              </ul>
            </li>
          <!--  <li><a href="why_join.html">Why Join</a></li>-->
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">Membership<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li><a href="membership_process.html">Membership Process</a></li>
                <li><a href="team_committee.html">Membership Committee</a></li>
                <li><a href="signup.html">Apply to Tomouh</a></li>
                <li class=""><a href="faq.html">FAQs</a></li>
              </ul>
            </li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">The Latest<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li class=""><a href="media.html">Media</a></li>
                <li><a href="blog.html">Blog</a></li>
              </ul>
            </li>
            <li><a href="event.html">Events</a></li>
            <li class=""><a href="contact_us.html">Contact</a></li>
            <li class="login_signup"><a href="signup.html" class="signup">Sign Up</a></li>
            <li class="login_signup"><a href="login.html" class="signup">Login</a></li>
          </ul>
        </nav>
      </div>
    </header>
  </div>
</div>
<div class="wrapper">
  <div class="home_slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active" style="background-image: url('<?php echo base_url(); ?>assets/client/images/banner.png');">
          <div class="container">
            <div class="carousel-caption">
              <h3>"Part of what drives us to be better at what we do is a sense of 'lack' or 'shortage'. "</h3>
              <p>- Mounira Jamjoom</p>
            </div>
          </div>
        </div>
        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/client/images/banner.png');">
          <div class="container">
            <div class="carousel-caption">
              <h3>"Part of what drives us to be better at what we do is a sense of 'lack' or 'shortage'. "</h3>
              <p>- Mounira Jamjoom</p>
            </div>
          </div>
        </div>
        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/client/images/banner.png');">
          <div class="container">
            <div class="carousel-caption">
              <h3>"Part of what drives us to be better at what we do is a sense of 'lack' or 'shortage'. "</h3>
              <p>- Mounira Jamjoom</p>
            </div>
          </div>
        </div>
      </div>
      
      <!-- Left and right controls --> 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
  </div>
  <section class="red_section">
    <p class="white-text">"To enable achievers to achieve more than they ever thought possible"</p>
  </section>
  <section class="map_section">
    <div class="map_header">Our Members</div>
    <div id="map"></div>
    <div class="map_inner_section">
        <div class="center-block">
            <div class="west">
                <h1 class="number">48</h1>
                <p>Total Member</p>
            </div>
            <!--<div class="middle">
                 <h1 class="number">20</h1>
                <p>Count</p>
            </div>-->
           <!-- <div class="east">
                 <h1 class="number">22</h1>
                <p>EAST</p>
            </div>-->
        </div>
    </div>
  </section>
  <section class="video_section"> <a href="#myModal"  data-toggle="modal">
    <div class="bg_img">
      <div class="overlay">
        <div class="container">
          <div class="center-block"> <img src="<?php echo base_url(); ?>assets/client/images/play-btn.png" class="img-responsive" alt="">
            <h3>Video of the Month</h3>
            <p>Tomouh Q&A with HRH Prince Turki Al Faisal</p>
          </div>
        </div>
      </div>
    </div>
    </a> </section>
  <section class="member_section">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="member_updates">
            <div class="head">
              <h1>Member Updates</h1>
            </div>
            <div class="content-part">
              <div class="member_row">
                <p class="title">Mohammed ElKuwaiz </p>
                <p class="desc">has Become the Chairman Capital Market Authority.</p>
                <p class="date">2  DEC 2017</p>
              </div>
              <div class="member_row">
                <p class="title">Muhammad Alshiha </p>
                <p class="desc">is now a Member of the Board of Directors at Shuqaiq International Water and Electricity Company SIWEC.</p>
                <p class="date">1  DEC 2017</p>
              </div>
              <div class="member_row">
                <p class="title">Mohammed ElKuwaiz </p>
                <p class="desc">has Become the Chairman Capital Market Authority.</p>
                <p class="date">30  NOV 2017</p>
              </div>
              <div class="member_row">
                <p class="title">Mohammed ElKuwaiz </p>
                <p class="desc">has Become the Chairman Capital Market Authority.</p>
                <p class="date">29  NOV 2017</p>
              </div>
              <div class="btn_cls">
                <a href="membership.html" class="btn gray-btn">View all</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="member_month">
            <div class="head">
              <h1>Member of the Month</h1>
            </div>
            <div class="content-part">
              <p class="member_date">Feb 2017 </p>
              <div class="member_img"> <img src="<?php echo base_url(); ?>assets/client/images/member.png" class="img-responsive img-circle" alt=""> </div>
              <h1 class="member_name">Omar Hamadeh</h1>
              <p class="member_post">Vice President, Bassam Trading Company <br>
                Riyadh, Saudi Arabia</p>
            </div>
            <div class="btn_cls">
              <button type="button" class="btn gray-btn">Read more</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  <section class="bottom_slider">
  <div class="container">
      <div class="head white-text ">
              <h1>Recommended by Members</h1>
            </div>
      <div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book1.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Power, Politics and Culture</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book2.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Good to Great</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book3.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Creating Magic</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book4.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">How To Win Friends and Influence People</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
       
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book1.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Power, Politics and Culture</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book2.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Good to Great</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book3.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">Creating Magic</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                 <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/client/images/book4.png" class="img-responsive" alt="">
                 </div>
              </div>
              <div class="border"></div>
              <div class="red-text">How To Win Friends and Influence People</div>
              <p class="black-text">llins, recommended by Yazeed Al Mubarak</p>
          </div>
        </div>
      </div>
      <div class="owl-prev" id="prev">  </div>
      <div class="owl-next" id="next"> </div>
    </div>
    </section>
  
  
  
  
  <section class="info_section">
     <div class="container">
              <div class="row">
                 <div class="col-md-9 col-sm-12 col-xs-12 center-block">
                  <div class="col-sm-3 img-part">
                      <img src="<?php echo base_url(); ?>assets/client/images/circle_img.png" class="img-circle img-responsive" alt="">
                  </div>
                  <div class="col-sm-9 img-content">
                      <p class="first_row">"helping people who don't necessarily have the web development skills to connect directly to consumers. I see that as real shift in the opportunities available."</p>
                      <p class="sec_row">Patrick Hennry - Co Founder of Doco Cade</p>
                  </div>
              </div>
         </div>
      </div> 
  </section>
</div>
<div class="footer">
    <footer>
        <div class="container">
            <div class="first-part">
               <div class="col-sm-4 col-xs-12  col-lg-3">
                   <div class="first-col">
                         <div class="footer-logo"><img src="<?php echo base_url(); ?>assets/client/images/graylogo@2x.png" class="img-responsive" alt=""></div>
                        <ul>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        </ul>
                        
                    </div>
                    <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-5  col-xs-12">
                    <div class="sec-col">
                        <ul>
                           <li class=""><a href="about_us.html">About Tomouh</a></li>
                          <li><a href="team.html">Team</a></li>
                          <li><a href="membership_process.html">Membership</a></li>
                          <li class=""><a href="media.html">Media</a></li> 
                          <li class=""><a href="blog.html">Blog</a></li>
                          <li><a href="event.html">Events</a></li>
                        </ul>
                    </div>
                  <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-4  col-xs-12">
                    <div class="third-col">
                        <ul>
                            <li><a href="contact_us.html">Contact Us</a></li>
                            <li><a href="mailto:info@tomouh.net">info@tomouh.net</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            <div class="sec-part">
                <p>Tomouh Limited 2017. All Rights Reserved | Terms and Conditions </p>
            </div>
        
    </footer>
</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog video_modal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Tomouh Q&A with HRH Prince Turki Al Faisal</h4>
      </div>
      <div class="modal-body">
        <div class="videoWrapper">
          <iframe src="https://www.youtube.com/embed/wYUviw6T6W4" allowfullscreen="" width="640" height="480" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery --> 
<script src="<?php echo base_url(); ?>assets/client/js/jquery.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url(); ?>assets/client/js/bootstrap.min.js"></script> 
  <!--slider js-->

    <script src="<?php echo base_url(); ?>assets/client/js/jquery-mobile.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/webapplinks.js"></script> 
<script>
    
  	
	  // media query event handler
		var mql = window.matchMedia("screen and (max-width: 767px)");
		if (mql.matches){ // if media query matches
		//alert("Window is 1199px or wider")
		$(document).ready(function() {
			$("#owl-demo").owlCarousel({
				margin:0,
				dots:false,
				loop :true,
				autoplay:true,
				slideSpeed:500, 
							  responsive:{
					0:{
						items:1
					},
                    375:{
						items:2
					},
					600:{
						items:4
					},
					1000:{
						items:4
					}     
				}
			  });
			});
		}
		else{
		// do something else
		}

    
	  </script> 
	  <script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/owl.carousel.js"></script> 
	  
<script>
function myMap() {
  var myCenter = new google.maps.LatLng(51.508742,-0.120850);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);

  var infowindow = new google.maps.InfoWindow({
    content: "<strong>Khalid Al-Ali!</strong><br>Qatar"
  });
  infowindow.open(map,marker);
}
</script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCb311Zi9JMgg53QgVb-abcgwJ4HYVqOr4&callback=myMap"></script> 
<script>
    $(window).scroll(function() {    
var scroll = $(window).scrollTop();
 if (scroll >= 100) {
  $("#menu").addClass("menufixed", 2000);
}  
var scroll = $(window).scrollTop();
 if (scroll <= 100) {
  $("#menu").removeClass("menufixed");
}
});
       $(window).scroll(function() {    
var scroll = $(window).scrollTop();
 if (scroll >= 100) {
  $("#menu1").addClass("menufixed", 2000);
}  
var scroll = $(window).scrollTop();
 if (scroll <= 100) {
  $("#menu1").removeClass("menufixed");
}
});
    </script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>
</body>
</html>
