<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<div class="after-login">
<div class="inner_wrapper">
  <div class="membership_page">
    <section class="event_cal">
      <div class="sec_banner">
        <div class="container">
          <div class="page_head">
            <h1 class="red-text text-center font36">members updates</h1>
          </div>
        </div>
        <section class="member_section">
          <div class="container">
            <div class="member_updates">
              <div class="content-part">
                <div class="row">
                  <?php 
                    $total_members = sizeof($members_update);
                    foreach($members_update as $member_update){
                  ?>
                  <div class="col-sm-6 col-xs-12">
                    <div class="member_row">
                      <p class="title"><?php echo $member_update['v_user_name']; ?></p>
                      <p class="desc"><?php echo $member_update['l_description']; ?></p>
                      <p class="date"><?php  $date = $member_update['d_added'];
                        echo date("jS F Y", strtotime($date)); ?></p>
                    </div>
                  </div>
                  <?php
                    }
                  ?>
                </div>
              </div>
              <?php 
                if($total_members <= 8){
              ?>
              <form action="" method="post">
                <div class="btn_cls">
                  <button type="submit" class="btn gray-btn" name="view_all">View all</button>
                </div>
              </form>
              <?php
                } 
              ?>
            </div>
          </div>
        </section>
      </div>
    </section>
  </div>
</div>

