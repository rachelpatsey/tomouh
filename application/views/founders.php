<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<style>
.center-align {
    text-align: center;
}
.white-box{
  min-height: 250px !important;
}
.img-center img{
  /* height: 90px !important; */
}
.black-text{
  font-size:18px !important; 
}
.inner_wrapper {
	margin-top: 118px;
	padding-top: 55px;
}
.bottom_slider .img-block {
	height: 80px;
	padding: 5px;
	text-align: center;
	display: flex;
	align-items: center;
  display: -webkit-box; 
  display: -webkit-flex; 
  display: -ms-flexbox; 
}

.bottom_slider .img-block img {
	margin: 0 auto;
	max-height: 75px;
	width: auto !important;
}
.bottom_slider .img-block {
	align-items: center;
	justify-content: center;
}
</style>
<div class="inner_wrapper">
  <div class="about_page">
    <section class="about_us">
      <div class="banner_section">
        <!-- <div class="sec_banner"> -->
        <div class="">
          <div class="container">
            <div class="page_head">
              <h1 class="red-text text-center font36"><?php if(isset($title)){echo $title;} ?></h1>
            </div>
            <p class="content"><?php if(isset($main_description)){echo $main_description;} ?></p>
          </div>
        </div>
      </div>
    </section>
  </div>
 
  <section class="bottom_slider">
    <div class="container">
      <!-- <div class="head white-text">
        <h1 class=""></h1>
      </div> -->
      <div id="owl-demo" class="owl-carousel owl-theme">
        <?php
          if(isset($founders)){
            $length = sizeof($founders);
            for($i=0;$i<$length;$i++){
        ?>
        <div class="item">
          <div class="white-box">
            <div class="img-block center-block">
              <div class="img-center"> <img src="<?php echo base_url(); ?>assets/images/<?php if (isset($founders[$i]['v_image'])){ echo $founders[$i]['v_image']; } ?>" class="img-responsive" alt=""  > </div>
            </div>
            <div class="border"></div>
            <a href="<?php if (isset($founders[$i]['v_link']) && $founders[$i]['v_link'] != ''){ echo $founders[$i]['v_link']; }else{ echo '#'; } ?>"  <?php if (isset($founders[$i]['v_link']) && $founders[$i]['v_link'] != ''){ echo 'target="_blank"'; } ?>><div class="red-text"><?php if (isset($founders[$i]['v_title'])){ echo $founders[$i]['v_title']; } ?></div></a>
            <p class="black-text"><?php if (isset($founders[$i]['v_member'])){ echo $founders[$i]['v_member']; } ?></p>
          </div>
        </div>
        <?php
          }
        }
        ?>
      </div>
      <div class="owl-prev" id="prev"> </div>
      <div class="owl-next" id="next"> </div>
    </div>
  </section>
 
</div>
<?php include('inc/footer.php') ?>    
