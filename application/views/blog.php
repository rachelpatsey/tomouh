<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<div class="inner_wrapper">
  <div class="blog_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36">Tomouh Blog</h1>
        </div>
        <div class="row">
          <?php 
            if(isset($blogs)){
              foreach($blogs as $blog){
          ?>
          <div class="col-sm-4 col-xs-12">
            <div class="blog">
              <a href="<?php echo base_url(); ?>blog/<?php echo $blog['v_slug'];?>">	
              <div class="blog_img" style="background-image: url('<?php echo base_url(); ?>assets/images/<?php if(isset($blog['v_image'])){ echo $blog['v_image']; }?>');"></div>
              </a>
              <a href="<?php echo base_url(); ?>blog/<?php echo $blog['v_slug'];?>">
              <p class="blog_name spectral-font"><?php if(isset($blog['v_title'])){ echo $blog['v_title']; }?></p>
              </a>
              <p class="blog_date gray-text font14"><?php if(isset($blog['d_added'])){ 
                        $date = $blog['d_added'];
                        echo date("jS F, Y", strtotime($date));
                      }     ?>                     
              </p>
              <div class="blog_desc"><?php if(isset($blog['l_description'])){ 
			  echo '<p>'.substr(strip_tags($blog['l_description']),0, 200).'</p>'; }?></div>
            </div>
          </div>
          <?php
            }
          } 
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
include("inc/footer.php");
?>