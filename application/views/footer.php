<?php
$url = $_SERVER['REQUEST_URI'];
// $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$data= array();
$query = $this->db->query("SELECT * FROM tbl_sitesetting");
    $results = $query->result_array();

    foreach($results as $result){

      $data[strtolower($result['v_name'])] = $result['l_value'];

    }
?>

<div class="footer">
    <footer>
        <div class="container">
            <div class="first-part">
               <div class="col-sm-4 col-xs-12  col-lg-3">
                   <div class="first-col">
                         <div class="footer-logo"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $data['site_logo']; ?>" class="img-responsive" alt=""></div>
                        <ul>
                            <li><a href="<?php echo $data['instagram_link']; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo $data['linkedin_link']; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo $data['twitter_link']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo $data['youtube_link']; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        </ul>
                        
                    </div>
                    <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-5  col-xs-12">
                    <div class="sec-col">
                        <ul>
                          <li class="<?php if(strpos($url, 'about_us') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>about_us">About Tomouh</a></li>
                          <li class="<?php if(strpos($url, 'team') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>team">Team</a></li>
                          <li class="<?php if(strpos($url, 'membership_process') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>membership_process">Membership</a></li>
                          <li class="<?php if(strpos($url, 'media') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>media">Media</a></li> 
                          <li class="<?php if(strpos($url, 'blog') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>blog">Blog</a></li>
                          <li class="<?php if(strpos($url, 'event') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>event">Events</a></li>
                        </ul>
                    </div>
                  <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-4  col-xs-12">
                    <div class="third-col">
                        <ul>
                            <li class="<?php if(strpos($url, 'contact_us') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>contact_us">Contact Us</a></li>
                            <li><a href="mailto:<?php echo $data['contact_info']; ?>"><?php echo $data['contact_info']; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            <div class="sec-part">
                <p><?php echo $data['footer_text']; ?>  | Terms and Conditions </p>
            </div>
        
    </footer>
</div>