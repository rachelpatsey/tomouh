<?php 
include('inc/header_top.php');
include('inc/header.php'); 
?>
<style type="text/css">
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
</style>
<div class="account_page" style="margin-top: 100px;" id="payment_option">
      <?php 
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                <div class="container">
                <div id="response" class="error_box" style="padding-left:0px !important;padding-right:0px !important;">
                <?php  if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
              	</div>
              	</div>    
              <?php  }
              }?>
                          <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>  
              <section class="event_cal">
                  <div class="sec_banner" style=" <?php if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>padding-top: 20px !important;<?php }else{ ?>padding-top: 50px !important;<?php } ?> ">
                    <div class="container">
                       <div class="border-box">
                          <div class="row">
                            <div class="col-xs-12">
                          <h3 style="font-size: 25px;">Hello <?php if(isset($name)){ echo $name.','; } ?></h3><br>
                          <h5 style="">Please Upload your CV here (.doc, .docx, .pdf).</h5>
                          <form action="" method="post" enctype="multipart/form-data">
                      <div class="row" style="margin-top: 10px;">
                              <div class="col-sm-12">
                                  <div class="form-group">
                                <input type="file" name="cv" class="btn" style="float: left;" accept=".doc,.docx,.pdf">
                                <input type="submit" name="submit" class="btn red-btn" value="Submit">
                              </div>
                            </div>
                          </div>  
                          </form> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
</div>
<?php include('inc/footer.php'); ?>