<?php 

include("inc/header_top.php");

include("inc/header.php"); 

?>
<style type="text/css">
  strong {
    font-weight: 800 !important;
  }
   em {
    font-style: italic !important;
  }
    h1{
  font-size:2em;
  font-weight:bold;
  }
  h2{
  font-size:1.5em;
  font-weight:bold;
  }
  h3{
  font-size:1.17em;
  font-weight:bold;
  }
  h4{
  font-weight:bold;
  }
  h5{
  font-size:0.83em;
  font-weight:bold;
  }
  h6{
  font-size:0.67em;
  font-weight:bold;
  }
</style>
<div class="inner_wrapper">

  <div class="event_page">

    <section class="event_cal">

      <div class="sec_banner">

        <div class="container">

          <div class="page_head">

            <h1 class="red-text text-center font36"><?php if(isset($title)){ echo $title; } ?></h1>

          </div>

          <div class="team-div">

            <div class=""><?php if(isset($main_description)){ echo $main_description; } ?></div>

          </div>

          <div class="team-container">

            <div class="col-sm-12 col-xs-12">

              <div class="row position-relative">

                <?php 

                $i = 1;

                $j=0;

                if(isset($board_members)){

                  $a = count($board_members);

                  $c = floor($a/3);

                  $d = 1;

                  $b = 0;

                  if($a % 3 != 0){

                    $b=1;

                  }

                  foreach ( $board_members as $board_member ) {

                    $name_length = strlen($board_member['v_name']);

                    $length = 100;

                    if($name_length > 26){

                      $length = 65;

                    }

                    if(isset($board_member['l_description'])){

                      $position = $board_member['l_description'];

                      if (strlen($position) > $length ) {

                          $stringCut = substr($position, 0, $length);

                          $position = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';

                      } 

                    }else{

                      $position = '';

                    }

                  if($j%3 == 0){ 

                  ?>

                    </div>

                    <div class="row position-relative <?php if($d > $c && $b == 1){ ?>text_align_class<?php } ?>">

                <?php 

                     $d++;

                     } 

                ?>

                <div class="col-sm-4 team-member img-click" id="img-click<?php echo $i; ?>">

                  <div class="member-box">

                    <div class="member-pic" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($board_member['v_image'])){ echo $board_member['v_image']; } ?>'); margin-bottom: 30px;"></div>

                    <h2 class="red-text spectral-font name"><?php if(isset($board_member['v_name'])){ echo $board_member['v_name']; } ?></h2>

                    <div class="post">

                      <span><?php echo strip_tags($position); ?></span>

                      <div class="clearfix"></div>

                      <a class="arrow"></a>

                    </div>

                  </div>

                </div>

                <div class="pop-up" id="pop-up<?php echo $i; ?>">

                  <div class="pop-up-bg"> <a class="close-img" id="img-close<?php echo $i; ?>"></a>

                    <div class="col-sm-4 no-padding">

                      <div class="member-pic" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($board_member['v_image'])){ echo $board_member['v_image']; } ?>')"></div>

                    </div>

                    <div class="col-sm-8 member-desc">

                      <div class="col-sm-12 no-padding">

                        <div class="desc">

                          <h2 class="red-text spectral-font name"><span><?php if(isset($board_member['v_name'])){ echo $board_member['v_name']; } ?></span></h2>

                          <div class="post"><span><?php if(isset($board_member['v_position'])){ echo $board_member['v_position']; } ?></span></div>

                          <p><?php if(isset($board_member['l_description'])){ echo $board_member['l_description']; } ?></p>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                 <?php

                      $i++;

                      $j++;

                    }

                  } 

                ?>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

  </div>

</div>

<div id="pop-overlay"></div>



<?php include('inc/footer.php') ?>