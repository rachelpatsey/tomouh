<?php 
include('inc/header_top.php');
include('inc/header.php');	
?>
<div class="account_page" style="margin-top: 100px;" id="payment_option">  
  <section class="event_cal">
      <div class="sec_banner">
        <div class="container">
        	 <div class="border-box">
            	<div class="row">
              	<div class="col-xs-12">
                <?php if( isset($is_member) && $is_member == 1 ){ ?>

                  <h2 style="font-size: 25px;">You Subscribed Our Plan Successfully. Please Click Below Button For Login.</h2>
                  <a href="<?php echo base_url(); ?>login"><button style="margin-top: 20px;margin-right: 20px;" class="btn red-btn pull-right">Login</button></a>

                <?php } else { 

                  $contact_info = $this->tomouh_model->getSetting('CONTACT_INFO');

                  if( !isset( $contact_info ) || $contact_info == '' ) $contact_info = 'info@tomouh.net';

                ?>  
                
                  <h2 style="font-size: 25px;line-height: 1.3;">Thank you for submitting the payment. The next step is to verify your email address and a verification email will follow shortly. Please check your spam/junk folder if you don't receive a verification email or write us at <a href="mailto:<?php echo $contact_info; ?>"> <?php echo $contact_info; ?></a>.</h2>
        	      
                <?php } ?>
        </div>
    </div>
</section>
</div>
<?php include('inc/footer.php'); ?>