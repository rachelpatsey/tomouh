<?php 

include("inc/header_top.php");

include("inc/header.php"); 

?>

<div class="inner_wrapper">

  <div class="media_page">

          <div class="sec_banner">

             <div class="container">

                  <div class="page_head">

                      <h1 class="red-text text-center font36" style="font-family:'Montserrat', sans-serif">
					  <?php if(isset($category_name)){ echo $category_name; } ?></h1>

                  </div>

            <div class="media_tab">

                  <div class="tab-content">

                  	

                    <div id="photos" class="tab-pane fade in active">

                     <div class="col-xs-12">

                          <div class="btns center-block pull-right">

                              <a href="<?php echo base_url(); ?>media" class="btn gray-btn">Back</a>

                          </div>

                          </div>

                      <div class="photos_div">

                         <div class="row">
                         
                          <div class="col-md-12">
                          	<p><?php echo $category_desc;?></p>
                          </div>	

                          <?php

                            if(isset($childs)){

                              foreach($childs as $photo){

                          ?>

                          <div class="col-sm-4 col-xs-6 media-img">

                             <a href="<?php echo base_url(); ?>media/photos/<?php echo $photo['v_slug']; ?>/" gray-border>

                              <div class="single-img" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($photo['v_image'])){ echo $photo['v_image']; } ?>');"></div>

                              <div class="category-name  red-text"><?php if(isset($photo['v_name'])){ echo $photo['v_name']; } ?></div>

                              </a>

                          </div>

                          <?php

                            }

                          }

                          ?>

                          </div>

                         <?php
                         if(empty($childs)){?> 
							
                      	 <ul class="row">

                          <?php

                            if(isset($photos)){

                              foreach($photos as $photo){

                          ?>

                          <li class="col-sm-4 col-xs-6 media-img">

                              <div class="single-img" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($photo['v_image'])){ echo $photo['v_image']; } ?>');"></div>

                              <img src="<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($photo['v_image'])){ echo $photo['v_image']; } ?>" class="img-responsive img-position">

                          </li>

                          <?php 

                              }

                            }

                          ?>                          

                          </ul>

                          <?php }?> 

                      </div>

                    </div>

                    

                  </div>

                </div>

             </div>

         </div>

  </div>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

      <div class="modal-dialog gallery_popup">

        <div class="modal-content">         

          <div class="modal-body">                

          </div>

        </div><!-- /.modal-content -->

      </div><!-- /.modal-dialog -->

    </div>

<script src="<?php echo base_url(); ?>assets/client/js/photo-gallery.js"></script>

<?php

include("inc/footer.php"); 

?>