<?php
    $query_params =array();
    $url = $meta['home_page_video'];
    $query_str = parse_url($url, PHP_URL_QUERY);
    parse_str($query_str, $query_params);  

    $latitude = json_encode($lan_lat); 

include("inc/header_top.php");
include("inc/header.php"); ?>

<style type="text/css">
  .video_section .video_main{
    position: relative;
    display: block;
  }
  .video_section .video_texts{
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    text-align: center;
  }
  .video_section .video_texts img{
    margin: 210px auto 0;
  }
</style>

</head>

<body>
<div class="wrapper">
  <div class="home_slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <?php
        // echo "<pre>";
        // print_r($sliders);exit();
        if(isset($sliders)){
            $length = sizeof($sliders);
            for($i=0;$i<$length;$i++){
         ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" <?php if($i == 0){ ?> class="active" <?php }?>></li>
        <?php
         }
        }
        ?>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
        if(isset($sliders)){
            $length = sizeof($sliders);
            for($i=0;$i<$length;$i++){
         ?>
        <div class="item<?php if($i == 0){?> active <?php } ?>" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($sliders[$i]['v_image'])){ echo $sliders[$i]['v_image']; }  ?>');">
          <div class="container">
            <div class="carousel-caption">
              <h3><?php if(isset($sliders[$i]['l_main_text'])){ echo $sliders[$i]['l_main_text']; } ?></h3>
              <p>- <?php if (isset($sliders[$i]['v_text'])){ echo $sliders[$i]['v_text']; } {
               
              } ?></p>
            </div>
          </div>
        </div>
        <?php
         }
        }
        ?>
      </div>
      
      <!-- Left and right controls --> 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
  </div>
  <section class="red_section">
    <p class="white-text"><?php if(isset($main_description)){ echo $main_description; } ?></p>
  </section>
  <section class="map_section">
    <div class="map_header">Our Members</div>
    <div id="map"></div>
   <!--  <div class="map_inner_section">
        <div class="center-block">
            <div class="west">
                <h1 class="number"><?php if(isset($total_member)){ echo $total_member; } ?></h1>
                <p>Total Member</p>
            </div>
        </div>
    </div> -->
  </section>
  <section class="video_section video_section_cstm"> <a class="video_main">
    <div class="videoWrapper">
     <iframe id="video" src="https://www.youtube.com/embed/<?php echo $query_params['v']; ?>?rel=0" allowfullscreen="" frameborder="0"></iframe>
     </div>
          <div class="video_texts">
            <img class="vd_poster" src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['home_page_video_image'])){ echo $meta['home_page_video_image']; } ?>" class="img-responsive play-btn" alt="">
            <div class="video_texts_cstm">
              <img src="<?php echo base_url(); ?>assets/frontend/images/video_play.png" class="img-responsive play-btn" alt="">
              <?php if(isset($meta['home_page_video_content'])){ echo $meta['home_page_video_content']; } ?>
            </div>
          </div>
          
    </a> </section>
  <section class="member_section">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="member_updates">
            <div class="head">
              <h1>Member Updates</h1>
            </div>
            <div class="content-part">
              <?php
                    for($i=0;$i<5;$i++){
              ?>
              <div class="member_row">
                <p class="title"><?php echo $members_update[$i]['v_user_name']; ?></p>
                <p class="desc"><?php echo $members_update[$i]['l_description']; ?></p>
                <p class="date"><?php  $date = $members_update[$i]['d_added'];
                        echo date("jS F Y", strtotime($date)); ?></p> 
              </div>
              <?php 
                }
              ?>
              <div class="btn_cls">
                <a href="<?php echo base_url().'member_updates'; ?>" class="btn gray-btn">View all</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="member_month">
            <div class="head">
              <h1>Member of the Month</h1>
            </div>
            <div class="content-part">
              <?php
                  $currentMonth = date('F');
                  $previous_month = Date('F', strtotime($currentMonth . " last month")); 
                  $current_year = date('Y');
                  if($previous_month == 'December'){
                      $current_year = $current_year - 1;
                  }
				  $MEMBER_IMAGE = $this->tomouh_model->getSetting('MEMBER_IMAGE');
				  $MEMBER_MAIN_OCCUPATION = $this->tomouh_model->getSetting('MEMBER_MAIN_OCCUPATION')
               ?>
              <p class="member_date"><?php echo $member_of_month['v_month_text']; ?></p>
              <div class="member_img"> 
              <?php if($MEMBER_IMAGE != ''){?>
              <img src="<?php echo base_url().'assets/frontend/images/'.$MEMBER_IMAGE;?>" class="img-responsive img-circle" alt="">
              <?php }else{?>
              <img src="<?php echo base_url(); ?>assets/images/no-photo-image.jpg" class="img-responsive img-circle" alt="">
              <?php }?> 
              </div>
              <h1 class="member_name"><?php if(isset($member_of_month['v_firstname'])) echo $member_of_month['v_firstname']; ?> <?php if(isset($member_of_month['v_lastname'])) echo $member_of_month['v_lastname']; ?></h1>
              <p class="member_post">
			  <?php if(isset($MEMBER_MAIN_OCCUPATION))
			  	 echo $MEMBER_MAIN_OCCUPATION.'<br>'; ?>          
			  <?php if(isset($member_of_month['v_job_title'])) 
			  	echo $member_of_month['v_job_title']; ?> 
			  <?php if(isset($member_of_month['v_company'])) 
			  	echo $member_of_month['v_company'].'<br>'; ?>
              <?php if(isset($member_of_month['v_residence_city'])) echo $member_of_month['v_residence_city']; ?><?php if(isset($member_of_month['v_residence_country']) && !empty($member_of_month['v_residence_country'])) echo ", ".$member_of_month['v_residence_country']; ?></p>
                
            </div>
            <?php 
			$MEMBER_OF_MONTH_LINK = $this->tomouh_model->getSetting('MEMBER_OF_MONTH_LINK');
			if($MEMBER_OF_MONTH_LINK != ''){
			?>
            <div class="btn_cls">
              <a href="<?php echo $MEMBER_OF_MONTH_LINK; ?>"><button type="button" class="btn gray-btn">Read more</button></a>
            </div>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  <section class="bottom_slider">
  <div class="container">
      <div class="head white-text ">
        <h1>Recommended by Members</h1>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme">

      <?php
        if(isset($books)){
          $length = sizeof($books);
        for($i=0;$i<$length;$i++){
      ?>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/frontend/images/<?php if (isset($books[$i]['v_image'])){ echo $books[$i]['v_image']; } ?>" class="img-responsive" alt="">
                </div>
              </div>
              <div class="border"></div>
              <div class="red-text"><?php if (isset($books[$i]['v_title'])){ echo $books[$i]['v_title']; } ?></div>
              <p class="black-text"><?php if (isset($books[$i]['l_description'])){ echo $books[$i]['l_description']; } ?></p>
          </div>
        </div>
      <?php 
        }
      }
      ?>
      </div>
      <div class="owl-prev" id="prev">  </div>
      <div class="owl-next" id="next"> </div>
    </div>
    </section>
  
  <section class="info_section">
      <div class="container">
              <div class="row">
                 <div class="col-md-9 col-sm-12 col-xs-12 center-block">
                  <div class="col-sm-3 img-part">
                      <img src="<?php echo base_url(); ?>assets/images/<?php if (isset($testimonial['v_image'])){ echo $testimonial['v_image']; } ?>" class="img-circle img-responsive" alt="">
                  </div>
                  <div class="col-sm-9 img-content">
                      <p class="first_row"><?php if (isset($testimonial['l_text'])){ echo $testimonial['l_text']; } ?></p>
                      <p class="sec_row"><?php if (isset($testimonial['v_name'])){ echo $testimonial['v_name']; } ?></p>
                  </div>
              </div>
         </div>
      </div> 
  </section>
</div>

<!-- Modal HTML -->
<!-- <div id="myModal" class="modal fade">
  <div class="modal-dialog video_modal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><?php if(isset($meta['home_page_video_title'])){ echo $meta['home_page_video_title']; } ?></h4>
      </div>
      <div class="modal-body">
        <div class="videoWrapper">
          <iframe src="https://www.youtube.com/embed/<?php echo $query_params['v']; ?>?rel=0&autoplay=0" allowfullscreen="" width="640" height="480" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
 -->
<script>
    // media query event handler
    var mql = window.matchMedia("screen and (max-width: 767px)");
    if (mql.matches){ // if media query matches
    //alert("Window is 1199px or wider")
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        margin:0,
        dots:false,
        loop :true,
        autoplay:true,
        slideSpeed:500, 
                responsive:{
          0:{
            items:1
          },
                    375:{
            items:2
          },
          600:{
            items:4
          },
          1000:{
            items:4
          }     
        }
        });
      });
    }
    else{
    // do something else
    }

    
    </script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/owl.carousel.js"></script> 
    <script type="text/javascript">
      
      $(document).ready(function(){
        $('.video_main').click(function(){
          $(this).removeClass('video_main');
          $("#video")[0].src += "&autoplay=1";
          $('.video_texts').css('display','none');
        });
      });    
    </script>
    
<script>

function myMap() {

  var lang_lat = '<?php echo $latitude; ?>';

  var locations = jQuery.parseJSON(lang_lat);

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: new google.maps.LatLng(21.5934402,39.1484342),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (i = 0; i < locations.length; i++) {  
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent("<strong>"+locations[i]['country_name']+"</strong><br>Total Members: "+locations[i]['total_members']);
        infowindow.open(map, marker);
      }
    })(marker, i));

  }

}

</script>
<?php echo "<script src='https://maps.googleapis.com/maps/api/js?key=".$google_map_key."&callback=myMap'></script>"; ?> 
<script>
    $(window).scroll(function() {    
var scroll = $(window).scrollTop();
 if (scroll >= 100) {
  $("#menu").addClass("menufixed", 2000);
}  
var scroll = $(window).scrollTop();
 if (scroll <= 100) {
  $("#menu").removeClass("menufixed");
}
});
       $(window).scroll(function() {    
var scroll = $(window).scrollTop();
 if (scroll >= 100) {
  $("#menu1").addClass("menufixed", 2000);
}  
var scroll = $(window).scrollTop();
 if (scroll <= 100) {
  $("#menu1").removeClass("menufixed");
}
});
    </script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>

<?php include('inc/footer.php') ?>