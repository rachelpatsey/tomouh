<?php include("inc/header_top.php"); ?>
<?php include("inc/header.php"); ?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>
  <style type="text/css">
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
  input.parsley-error {
    border: 1px solid red;
  }

  .parsley-error-list {
      color: red;
  }
</style>
<div class="inner_wrapper">
  <div class="contact_page">
           <?php 
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                <div id="response" class="error_box">
              <?php if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
                </div>
              <?php  }
              }?>
                          <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
  
    <section class="contact_us">
      <div class="sec_banner">
        <div class="container">
          <div class="page_head">
            <h1 class="red-text text-center font36"><?php if(isset($title)){ echo $title; }?></h1>
          </div>
          <p class="text"><?php if(isset($main_description)){ echo $main_description; }?></p>
          <div class="contact_details">
            <div class="row">
              <div class="col-sm-6 col-xs-12 left-part"> 
                <div class="email">
                  <div class="col-sm-12 col-xs-12">
                    <div class="row">
                      <div class="col-xs-3 col-sm-2"> <img src="<?php echo base_url(); ?>assets/client/images/email.png" class="img-responsive" alt=""> </div>
                      <div class="col-xs-9 col-sm-10 no-padding" >
                        <p class="red-text"><strong>Contact Info</strong></p>
                        <p><?php if(isset($contact_info)){ echo $contact_info; }?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xs-12 right-part"> 
                <!--<div class="inner_page_head">
                          <h1>Contact Form</h1>
                       </div>-->
                <form class="contact_form" action="" method="post" id="frm_contact_us" parsley-validate>
                  <div class="form-group">
                    <input class="form-control" placeholder="Name" name="v_user_name" id="name" type="text" parsley-required="true" parsley-minlength="4" parsley-maxlength="20">
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Email" name="v_user_email" id="email" type="text" parsley-required="true" parsley-type="email">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" rows="3" cols="6" name="l_user_message" id="l_message" placeholder="Message" parsley-required="true" parsley-minlength="20" parsley-maxlength="100"></textarea>
                  </div>
                  <div class="form-group">
                    <button class="btn red-btn" type="submit" name="btn_submit" value="CONTACT US">Send Message</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php include('inc/footer.php'); ?>
