<?php 
include("inc/header_top.php"); 
include("inc/header.php");
?>
<div class="after-login">
    <style type="text/css">

      .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
      .error_box .icon{float: left;padding-right: 10px;}
      input.parsley-error {
        border: 1px solid red;
      }

      .parsley-error-list {
          color: red;
      }
    </style>
<div>
  <div class="account_page" style="margin-top: 100px;">
           <?php 
              if(!($this->form_validation->error_array())){ 
    
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>

                <div id="response" class="error_box">
                 <?php  if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
                </div> 
              <?php  } 
               }?>
              <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>

    <section class="event_cal">
      <div class="sec_banner">
        <div class="container">
          <div class="page_head">
            <h1 class="red-text text-center font36">Forgotten Password</h1>
          </div>
          <div class="border-box">
            <div class="email-block">
              <div class="row">
                <div class="col-sm-12 col-xs-12">
                  <label>Enter the email address associated with your Tomouh account.</label>
                </div>
              </div>
            </div>
            <form action="" method="post" class="change_password" parsley-validate>
              <div class="row">
                <div class="col-xs-3">
                  <p class="form-head">Enter Your Email Address:</p>
                </div>
                <div class="col-xs-5" style="margin-top: 2.2%;">
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Email" parsley-required="true" parsley-type="email">
                  </div>
                </div>
                <div class="col-xs-3">
                  <button class="btn red-btn" type="submit" style="margin-top: 11%;margin-left: 10px;">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</div>

<?php include('inc/footer.php');?>

