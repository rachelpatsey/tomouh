<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<div class="inner_wrapper">
  <div class="about_page">
    <section class="about_us">
      <div class="banner_section">
        <div class="sec_banner">
          <div class="container">
            <div class="page_head">
              <h1 class="red-text text-center font36"><?php if(isset($title)){echo $title;} ?></h1>
            </div>
            <p class="content"><?php if(isset($main_description)){echo $main_description;} ?></p>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php include('inc/footer.php') ?>