<?php 

$tele_code = isset($user_step1['tele_country_code']) ? str_replace('+', '', $user_step1['tele_country_code']) : '';
$tele_2_code = isset($user_step1['tele_2_country_code']) ? str_replace('+', '', $user_step1['tele_2_country_code']) : '';

?>
<?php
include("inc/header_top.php");
include("inc/header.php");
?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>

<style type="text/css">
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
  input.parsley-error {
    border: 1px solid red;
  }
  .type{
    clear: left;
  }

  .parsley-error-list {
      color: red;
  }
  .eye_input{
    position: absolute;
    right: 22px;
    line-height: 37px;
    z-index: 99;
    top: 30px;
    color: #888;
    font-size: 18px;
  }
  .frm-cntl .select2 .select2-selection--single{
    box-shadow: none !important;
    height: 40px !important;
    border-radius: 0px;
    border: 1px solid #ccc !important;

  }
  .frm-cntl .select2-container--default .select2-selection--single .select2-selection__rendered{line-height: 40px;}
  .frm-cntl .select2-selection__clear{display: none;}
  .frm-cntl .select2-container--default .select2-selection--single .select2-selection__arrow{top: 7px;}
  .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: white !important;
    color: black !important;
  }
</style>

<div class="inner_wrapper">
              <?php
                  if(!($this->form_validation->error_array())){
                      if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                        <div id="response" class="error_box">
                        <?php  if($_GET['succ']==1){
                              echo $this->messages_model->getSuccessMsg($_GET['msg']);
                            }
                            else if($_GET['succ']==0){
                              echo $this->messages_model->getErrorMsg($_GET['msg']);
                            } ?>
                        </div>
                        <?php } 
                          }?>
              <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                  <div class="icon">
                                                      <i class="fa fa-times-circle"></i>
                                                  </div>', '</div>');?>
<div class="container">
  <div class="signup_page">
    <div class="form">
      <h2 class="signup-head red-text spectral-font font36 text-center"><strong>Register</strong></h2>
      <div class="signup_headline">
        <ul class="nav nav-tabs nav-justified">
          <li class="active"><a><img src="<?php echo base_url(); ?>assets/client/images/active-radio.png" class="img-responsive" alt="">PERSONAL</a></li>
          <li><a><img src="<?php echo base_url(); ?>assets/client/images/radio-default.png" class="img-responsive" alt="">EDUCATION & Professional</a></li>
        </ul>
      </div>
      <form name="signup_form_1" class="signup_form" method="post" enctype="multipart/form-data" parsley-validate>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Personal Information</p>
            </div>
          </div>
          <div class="row">
            <?php 

            //echo "<pre>";print_r($member); exit;
            ?>
            <div class="col-md-3">
              <div class="form-group">
                <label>First Name*</label>
                <input type="hidden" name="type" value="<?php if(isset($member['contact_id']) && !empty($member['contact_id'])){ echo"member"; }elseif(isset($user_step1['type']) && !empty($user_step1['type'])){ echo $user_step1['type']; }else{ echo"non_member"; } ?>">
                <input type="hidden" name="contact_id" value="<?php if(isset($member['contact_id']) && !empty($member['contact_id'])){ echo $member['contact_id']; }elseif(isset($user_step1['contact_id']) && !empty($user_step1['contact_id'])){ echo $user_step1['contact_id']; } ?>">
                <input type="hidden" name="plan_type" value="<?php if(isset($plan_type) && !empty($plan_type)){ echo $plan_type; }else{ echo " "; } ?>">
                <input type="text" name="firstname" id="firstnme" class="form-control" value="<?php if(isset($member['memberdata']['First_Name']) && !empty($member['memberdata']['First_Name'])){ echo $member['memberdata']['First_Name']; }elseif(isset($user_step1['firstname']) && !empty($user_step1['firstname'])){ echo $user_step1['firstname']; } ?>" parsley-required="true">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Last Name*</label>
                <input type="text" name="lastname" id="lastname" class="form-control" value="<?php if(isset($member['memberdata']['Last_Name']) && !empty($member['memberdata']['Last_Name'])){ echo $member['memberdata']['Last_Name']; }elseif(isset($user_step1['lastname']) && !empty($user_step1['lastname'])){ echo $user_step1['lastname']; } ?>" parsley-required="true">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Date Of Birth*</label>
                <div class="input-group date" data-provide="datepicker">
                    <input type="text" name="dob" id="dob" value="<?php if(isset($member['memberdata']['Date_of_Birth']) && !empty($member['memberdata']['Date_of_Birth'])){ echo date('m/d/Y', strtotime($member['memberdata']['Date_of_Birth'])); }elseif(isset($user_step1['dob']) && !empty($user_step1['dob'])){ echo date('m/d/Y', strtotime($user_step1['dob'])); } ?>" class="form-control" parsley-required="true">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Gender</label>
                <div class="select">
                  <select class="selectpicker" name="gender" id="gender">
                    <option <?php if(isset($member['memberdata']['Gender']) || isset($user_step1['gender'])){ if((!empty($member['memberdata']['Gender']) && $member['memberdata']['Gender'] == 'Male') || (!empty($user_step1['gender']) && $user_step1['gender'] == 'Male')){ ?> selected <?php } }?>>Male</option>
                    <option <?php if(isset($member['memberdata']['Gender']) || isset($user_step1['gender'])){ if((!empty($member['memberdata']['Gender']) && $member['memberdata']['Gender'] == 'Female') || (!empty($user_step1['gender']) && $user_step1['gender'] == 'Female')) { ?> selected <?php } }?>>Female</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Account</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Email*</label>
                <input type="text" name="email" id="email" value="<?php if(isset($member['memberdata']['Email']) && !empty($member['memberdata']['Email'])){ echo $member['memberdata']['Email']; }elseif(isset($user_step1['email']) && !empty($user_step1['email'])){ echo $user_step1['email']; } ?>" class="form-control" parsley-required="true" <?php if(isset($member['memberdata']['Email']) && !empty($member['memberdata']['Email'])){ ?> readonly="readonly" <?php } ?> parsley-type="email">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group pwd">
                <label>Password*</label>
                <input type="password" name="password" id="password" class="form-control" <?php if(!$this->session->has_userdata('logged_user')) {?> parsley-required="true" parsley-minlength="6" parsley-maxlength="18" <?php } ?>>
                <a class="eye_input" onclick="passwordVisible()"><i class="fa fa-eye" aria-hidden="true"></i></a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group pwd">
                <label>Confrim Password*</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control" <?php if(!$this->session->has_userdata('logged_user')) {?> parsley-required="true" parsley-minlength="6" parsley-maxlength="18" <?php } ?>>
                <a class="eye_input" onclick="confirmPasswordVisible()"><i class="fa fa-eye" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Contact Information</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-xs-12">
              <div class="form-group frm-cntl">
                <label>Home Country*</label>
                <!-- <div class="select"> -->
                  <select class="form-control" name="home_country" id="home_country" required="required">
                    <option value="">Select Country</option>
                    <?php foreach($countries as $country) {?>
                    <option <?php if(isset($member['memberdata']['Home_Country']) || isset($user_step1['home_country'])){ if((!empty($member['memberdata']['Home_Country']) && $member['memberdata']['Home_Country'] == $country['country_name']) || (isset($user_step1['home_country']) && !empty($user_step1['home_country']) && $user_step1['home_country'] == $country['country_name'])){ ?> selected <?php } }?>><?php echo $country['country_name']; ?></option>
                    <?php } ?>
                  </select>
                  <!-- <span id="home_country_error"><font color="#ff0000">This value is required.</font></span> -->
                <!-- </div> -->
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>Home City*</label>
                <input type="text" name="home_city" id="home_city" class="form-control" value="<?php if(isset($member['memberdata']['Home_City']) && !empty($member['memberdata']['Home_City'])){ echo $member['memberdata']['Home_City']; }elseif(isset($user_step1['home_city']) && !empty($user_step1['home_city'])){ echo $user_step1['home_city']; } ?>" parsley-required="true">
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group frm-cntl">
                <label>Country of Residence*</label>
                <!-- <div class="select"> -->
                  <select class="form-control" name="residence_country" id="residence_country" required="required">
                    <option value="">Select Country</option>
                     <?php foreach($countries as $country) {?>
                    <option <?php if(isset($member['memberdata']['Country_of_Residence']) || isset($user_step1['residence_country'])){ if((!empty($member['memberdata']['Country_of_Residence']) && $member['memberdata']['Country_of_Residence'] == $country['Country_of_Residence']) || (isset($user_step1['residence_country']) && !empty($user_step1['residence_country']) && $user_step1['residence_country'] == $country['country_name'])){ ?> selected <?php } }?>><?php echo $country['country_name']; ?></option>
                    <?php } ?>
                  </select>
                  <!-- <span id="residence_country_error"><font color="#ff0000">This value is required.</font></span> -->
               <!--  </div> -->
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>City of Residence*</label>
                <input type="text" name="residence_city" id="residence_city" class="form-control" value="<?php if(isset($member['memberdata']['City_Of_Residence']) && !empty($member['memberdata']['City_Of_Residence'])){ echo $member['memberdata']['City_Of_Residence']; }elseif(isset($user_step1['residence_city']) && !empty($user_step1['residence_city'])){ echo $user_step1['residence_city']; } ?>" parsley-required="true">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-6  col-xs-12">
              <div class="form-group frm-cntl">
                <label>Mobile Number*</label>
                <div class="row">
                  <div class="col-sm-12">
                   <div style="width: 30%;float: left;">
                      <select class="form-control" name="tele_country_code" id="tele_country_code" required="required">
                        <option value="">Code</option>
                        <?php foreach($countries as $country) {?>
                        <option <?php if(isset($tele_code) && $tele_code == $country['phonecode'] ){ ?> selected <?php } ?>>+<?php echo $country['phonecode']; ?></option>
                        <?php } ?>
                      </select>
                      <!-- <span id="tele_country_code_error"><font color="#ff0000">This value is required.</font></span> -->
                    </div>
                    <input type="text" style="width: 68%;float: right;" name="telephone" id="telephone" class="form-control" value="<?php if(isset($user_step1['telephone']) && !empty($user_step1['telephone'])){ echo $user_step1['telephone']; } ?>" parsley-type="number" parsley-minlength="6" parsley-maxlength="12" parsley-required="true">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
              <div class="form-group frm-cntl">
                <label>Mobile Number 2</label>
                <div class="row">
                  <div class="col-sm-12">
                   <div style="width: 30%;float: left;">
                      <select class="form-control tele_country" id="tele_2_country_code" name="tele_2_country_code">
                        <option value="">Code</option>
                        <?php foreach($countries as $country) {?>
                        <option id="<?php echo $country['country_name']; ?>" <?php if(isset($tele_2_code) && $tele_2_code == $country['phonecode'] ){ ?> selected <?php } ?>>+<?php echo $country['phonecode']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <input type="text" style="width: 68%;float: right;" name="telephone_2" id="telephone_2" class="form-control" value="<?php if(isset($user_step1['telephone_2']) && !empty($user_step1['telephone_2'])){ echo $user_step1['telephone_2']; } ?>" parsley-type="number" parsley-minlength="6" parsley-maxlength="12">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Social Account</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>Twitter Link</label>
                <input type="text" name="twitter_link" id="twitter_link" class="form-control" value="<?php if(!empty($member['memberdata']['v_twitter_link'])){ echo $member['memberdata']['v_twitter_link']; }elseif(isset($user_step1['twitter_link']) && !empty($user_step1['twitter_link'])){ echo $user_step1['twitter_link']; } ?>">
                <p class="gray-text font12"><a href="https://twitter.com/" target="_blank"> twitter.com/johndoe</a></p>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>Linkedin Link</label>
                <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" value="<?php if(!empty($member['memberdata']['v_linkedin_link'])){ echo $member['memberdata']['v_linkedin_link']; }elseif(isset($user_step1['linkedin_link']) && !empty($user_step1['linkedin_link'])){ echo $user_step1['linkedin_link']; } ?>">
                <p class="gray-text font12"><a href="https://uk.linkedin.com/" target="_blank"> uk.linkedin.com/in/johndoe</a></p>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>Instagram Link</label>
                <input type="text" name="instagram_link" id="instagram_link" class="form-control" value="<?php if(!empty($member['memberdata']['v_instagram_link'])){ echo $member['memberdata']['v_instagram_link']; }elseif(isset($user_step1['instagram_link']) && !empty($user_step1['instagram_link'])){ echo $user_step1['instagram_link']; } ?>">
                <p class="gray-text font12"><a href="https://www.instagram.com/" target="_blank"> www.instagram.com/johndoe</a></p>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="form-group member_select_bx">
                <label>Were you referred to by a member?</label>
                <div class="row">
                    <div class="select col-md-3 col-sm-4 col-xs-12">
                      <select class="selectpicker" name="referral" id="referral">
                        <option value="0" <?php if((!empty($member['memberdata']['e_referral']) && $member['memberdata']['e_referral'] == 0) || (isset($user_step1['referral']) && !empty($user_step1['referral']) && $user_step1['referral'] == 0)){ ?> selected <?php } ?>>No</option>
                        <option value="1" <?php if((!empty($member['memberdata']['e_referral']) && $member['memberdata']['e_referral'] == 1) || (isset($user_step1['referral']) && !empty($user_step1['referral']) && $user_step1['referral'] == 1)){ ?> selected <?php } ?>>Yes</option>
                      </select>
                    </div>
                    <div class="col-md-8 col-sm-8" id="referral_block" style="<?php if(!empty($user_step1['referral']) && $user_step1['referral'] == 1){ ?>display: block; <?php }else{ ?>display: none; <?php } ?>" >
                      <input type="email" name="referral_email" id="referral_email" placeholder="Email" class="form-control" value="<?php if(!empty($member['memberdata']['v_referral_email'])){ echo $member['memberdata']['v_referral_email']; }elseif(isset($user_step1['referral_email']) && !empty($user_step1['referral_email'])){ echo $user_step1['referral_email']; } ?>">
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
            <input type="submit" name="signup_1" id="signup_1" class="btn gray-btn" value="Continue">
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  $('#referral').change(function(){
        var ref = $('#referral').val();

        if(ref == 1){
          $('#referral_block').css('display','block');
        }else{
          $('#referral_block').css('display','none');
        }
  });
</script>
<script type="text/javascript">
  $('#residence_country').change(function(){

    var val = $(this).val();

    var ajax_url = '<?php echo base_url(); ?>getCountryFromName';
    $.ajax({
        url:ajax_url,
        type: 'POST',
        crossDomain: true,
        cache: false,
        dataType: 'json',
        data: {
          "country":val,
        },
        success: function(data){

          response_data = JSON.parse(JSON.stringify(data));

          if(response_data['phonecode']){

              // $('#tele_country_code').val('+'+response_data['phonecode']).trigger("change");
              $('#tele_2_country_code').val('+'+response_data['phonecode']).trigger("change");
          }
        }
    });
  });
   $('#home_country').change(function(){

    var val = $(this).val();

    var ajax_url = '<?php echo base_url(); ?>getCountryFromName';
    $.ajax({
        url:ajax_url,
        type: 'POST',
        crossDomain: true,
        cache: false,
        dataType: 'json',
        data: {
          "country":val,
        },
        success: function(data){

          response_data = JSON.parse(JSON.stringify(data));

          if(response_data['phonecode']){

              $('#tele_country_code').val('+'+response_data['phonecode']).trigger("change");
             
          }
        }
    });
  });
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>

<script>
function passwordVisible() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function confirmPasswordVisible() {
    var x = document.getElementById("confirm_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

</script>
<script type="text/javascript">
 $("#home_country, #residence_country, #tele_country_code, #tele_2_country_code").select2({
    allowClear: true,
    width: "resolve"
  });
</script>

<script>
    $( function() {
      $( "#dob" ).datepicker();
    } );
  </script>
<?php
include("inc/footer.php");
?>