<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<div class="inner_wrapper">
  <div class="media_page">
          <div class="sec_banner">
             <div class="container">
                  <div class="page_head">
                      <h1 class="red-text text-center font36">Media</h1>
                  </div>
            <div class="media_tab">
                  <ul class="nav nav-tabs nav-justified">
                    <li <?php if(isset($type) && $type == 'image'){ ?> class="active" <?php } ?>><a data-toggle="tab" href="#photos">Photos</a></li>
                    <li <?php if(isset($type) && $type == 'video'){ ?> class="active" <?php } ?>><a data-toggle="tab" href="#video">Videos</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="photos" class="tab-pane fade <?php if(isset($type) && $type == 'image'){ ?>in active<?php } ?>">
                      <div class="photos_div">
                         <div class="row">
                          <?php
                            if(isset($photos)){
                              foreach($photos as $photo){
                          ?>
                          <div class="col-sm-4 col-xs-6 media-img">
                             <a href="<?php echo base_url(); ?>media/photos/<?php echo $photo['v_slug']; ?>/" gray-border>
                              <div class="single-img" style="background-image: url('<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($photo['v_image'])){ echo $photo['v_image']; } ?>');"></div>
                              <div class="category-name  red-text"><?php if(isset($photo['v_name'])){ echo $photo['v_name']; } ?></div>
                              </a>
                          </div>
                          <?php
                            }
                          }
                          ?>
                          </div>
                          <?php 
                            $total_page = ceil($total_row/$limit);
                            $next_page = $page + 1;
                            $pre_page = $page - 1;
                          ?>
                          <div class="pagination_div"> <a <?php if(isset($page) && $page == 1){  }else{?> href="<?php echo base_url(); ?>media?page=<?php echo $pre_page; ?>&type=image" <?php } ?> class="gray-border-btn back-btn">Back</a>
                            <ul class="custom_pagination">
                               <?php
                                for( $i=1;$i<=$total_page;$i++){
                               ?>
                              <li <?php if($page == $i){ ?> class="active" <?php } ?>><a class="form_submit_page" href="<?php echo base_url(); ?>media?page=<?php echo $i; ?>&type=image"><?php echo $i; ?></a></li>
                              <?php } ?>
                       <!--        <li class="active"><a href="#">2</a></li> -->
                            </ul>
                            <a <?php if(isset($page) && $page == $total_page){ }else{?> href="<?php echo base_url(); ?>media?page=<?php echo $next_page; ?>&type=image" <?php } ?>  class="gray-border-btn next-btn">Next</a>
                          </div>
                      </div>
                    </div>
                    <div id="video" class="tab-pane fade <?php if(isset($type) && $type == 'video'){ ?>in active<?php } ?>">
                        <div class="videos_div">
                            <div class="row">
                              <?php
                                if(isset($videos['result'])){
                                  foreach($videos['result'] as $video){

                                    $query_params =array();
                                    $url = $video['v_url'];
                                    $query_str = parse_url($url, PHP_URL_QUERY);
                                    parse_str($query_str, $query_params);                          

                              ?>
                              <div class="col-sm-4 col-xs-6 media-img">
                                <iframe src="https://www.youtube.com/embed/<?php echo $query_params['v']; ?>?rel=0&autoplay=0" allowfullscreen="" width="100%" height="350" frameborder="0"></iframe>
                                 <a href="<?php echo base_url(); ?>media/videos/<?php echo $video['id'];?>/" >
                                  <img src="<?php echo base_url(); ?>assets/frontend/images/video_play.png" class="img-responsive play-btn" alt="">
                                 </a>
                                 <div class="video-title red-text"><?php if(isset($video['v_name'])){ echo $video['v_name']; } ?></div>
                              </div>
                              <?php
                                }
                              }
                              ?>                               
                            </div>
                            <?php 
                            $total_page = ceil($videos['total_row']/$limit);
                            $next_page = $page + 1;
                            $pre_page = $page - 1;
                          ?>
                          <div class="pagination_div"> <a <?php if(isset($page) && $page == 1){  }else{?> href="<?php echo base_url(); ?>media?page=<?php echo $pre_page; ?>&type=video" <?php } ?> class="gray-border-btn back-btn">Back</a>
                            <ul class="custom_pagination">
                               <?php
                                for( $i=1;$i<=$total_page;$i++){
                               ?>
                              <li <?php if($page == $i){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>media?page=<?php echo $i; ?>&type=video"><?php echo $i; ?></a></li>
                              <?php } ?>
                       <!--        <li class="active"><a href="#">2</a></li> -->
                            </ul>
                            <a <?php if(isset($page) && $page == $total_page){ }else{?> href="<?php echo base_url(); ?>media?page=<?php echo $next_page; ?>&type=video" <?php } ?>  class="gray-border-btn next-btn">Next</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
             </div>
         </div>
  </div>
</div>
<?php 
include("inc/footer.php"); 
?>