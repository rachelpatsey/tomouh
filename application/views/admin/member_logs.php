<?php
$current_year = date("Y");
?>
<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>
<style type="text/css">
	.select2 {
		width: 100%;
	}

	.select2-results .select2-result-label {
		padding: 7px 7px 9px 5px;
	}

	.borderless td, .borderless tr {
    border: none;
		border-top : none !important;
}

</style>
<div id="cl-wrapper" class="fixed-menu">
	<?php include("inc/sidebar.php"); ?>
	<div class="container-fluid" id="pcont">
		<!-- <div class="page-head">
			<h2><?php // echo $page_title; ?></h2>
		</div> -->
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">
							<h3>
								Member Logs
							</h3>
						</div>
						<div id="response">
							<?php
							if (!($this->form_validation->error_array())) {
								if (isset($_GET['msg']) && $_GET['msg'] != '') {
									if ($_GET['succ'] == 1) {
										echo $this->messages_model->getSuccessMsg($_GET['msg']);
									} else if ($_GET['succ'] == 0) {
										echo $this->messages_model->getErrorMsg($_GET['msg']);
									}
								}
							} ?>
							<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>'); ?>
						</div>
						<?php
						if (($script == 'add') || ($script == 'edit')) { ?>
						</div>
					<?php } else { ?>

						<div class="row">
							<div class="col-md-12">
								<div class="content">
									<?php
									$datedata = [];
									if (isset($rows) && count($rows)) {
										foreach ($rows as $key => $value) {
											$datedata[date('Y-m-d', strtotime($value['d_modified']))][] = $value;
										}
									}
									?>
									<table class="table table-borderless borderless">
									<?php
									if (isset($datedata) && count($datedata)) {
										foreach ($datedata as $date => $results) {
											echo '<tr><td colspan="2"><h3>'.date('jS F, Y',strtotime($date)).'</h3></td></tr>';
											foreach ($results as $key => $value) {
												$Memberdata = json_decode($value['l_data']);
												$olddata = json_decode($value['l_old_data']);
												$newdata = json_decode($value['l_new_data']);
												$firstname = $Memberdata->firstname;
												$lastname = $Memberdata->lastname;


												if (isset($Memberdata->gender) && $Memberdata->gender != '') {
													if ($Memberdata->gender == 'Male') $g_text = 'his';
													if ($Memberdata->gender == 'Female') $g_text = 'her';
												}
												
												if(count($newdata) && !empty($newdata)){
													foreach ($newdata as $k => $val) {
														if ($olddata->$k != '') {
															echo "<tr><td width='8%'>". date('g:i A', strtotime($value['d_modified']))."</td><td>" . $firstname . " " . $lastname . " updated " . $g_text . " " . $k . " From " . $olddata->$k . " to " . $val . "</td></tr>";
														} else {
															$k = str_replace('_',' ',$k);
															echo "<tr><td width='8%'>". date('g:i A', strtotime($value['d_modified']))."</td><td>" . $firstname . " " . $lastname . " updated " . $g_text . " " . $k . " : " . $val . "</td></tr>";
														}
													}
												}
												else{
													if($value['is_education'] == 1){
														echo "<tr><td width='8%'>". date('g:i A', strtotime($value['d_modified']))."</td><td>" . $firstname . " " . $lastname . " updated " . $g_text . " educational information : University :". $olddata->university.', Degree :' .$olddata->degree.', Major : '.$olddata->major.', Year : '.$olddata->passing_year." To Empty </td></tr>";
														
													}
													if($value['is_professional'] == 1){
														echo "<tr><td width='8%'>". date('g:i A', strtotime($value['d_modified']))."</td><td>" . $firstname . " " . $lastname . " updated " . $g_text . " professional information : Company :" . $olddata->company.', Job title : ' .$olddata->job_title.', Industry : '.$olddata->industry.', Period : '.$olddata->company_from.'-'.$olddata->company_to." To Empty </td></tr>";
													}
													if($value['is_achievements'] == 1){
														echo "<tr><td width='8%'>". date('g:i A', strtotime($value['d_modified']))."</td><td>" . $firstname . " " . $lastname . " updated " . $g_text . " achievements information : ". $olddata->achievement." To Empty </td></tr>";
													
													}
												}
											

											}
										}
									}
									?>
									</table>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


