<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>
<div id="cl-wrapper" class="fixed-menu">
  	<?php include("inc/sidebar.php"); ?>

  	<div class="container-fluid" id="pcont">
  		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		<div class="cl-mcont">
			<div class="row">
       			<div class="col-lg-3 col-xs-6">
	 	         	<div class="small-box bg-aqua">
            			<div class="inner">
              				<h3><?php if(isset($total_member)){ echo $total_member; } ?></h3>
              				<p>Total Member</p>
	            		</div>
            			<div class="icon">
              				<i class="ion ion-bag"></i>
            			</div>
            			<a href="<?php echo base_url().'admin/members/';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          			</div>
        		</div>
        		<!-- ./col -->
        		<div class="col-lg-3 col-xs-6">
          		<!-- small box -->
          			<div class="small-box bg-green">
            			<div class="inner">
              				<h3><?php if(isset($active_member)){ echo $active_member; } ?></h3>
				            <p>Active User</p>
            			</div>
	            		<div class="icon">
              				<i class="ion ion-stats-bars"></i>
            			</div>
            				<a href="<?php echo base_url().'admin/members/?status=active';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          			</div>
        		</div>
        		<!-- ./col -->
        		<div class="col-lg-3 col-xs-6">
          		<!-- small box -->
          			<div class="small-box bg-yellow">
            			<div class="inner">
              				<h3><?php if(isset($inactive_member)){ echo $inactive_member; } ?></h3>
              				<p>Inactive User</p>
            			</div>
            			<div class="icon">
              				<i class="ion ion-person-add"></i>
            			</div>
            				<a href="<?php echo base_url().'admin/members/?status=inactive';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          			</div>
        		</div>
        		<!-- ./col -->
        		<div class="col-lg-3 col-xs-6">
          		<!-- small box -->
          			<div class="small-box bg-red">
            			<div class="inner">
              				<h3><?php if(isset($total_payment)){ echo $total_payment; } ?></h3>
          					<p>Total Payment</p>
            			</div>
            			<div class="icon">
              				<i class="ion ion-pie-graph"></i>
            			</div>
            				<a href="<?php echo base_url().'admin/manage_payment/';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      				</div>
        		</div>
        		<!-- ./col -->
      		</div>
		</div>
  	</div>
</div>


