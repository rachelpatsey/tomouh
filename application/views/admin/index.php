<?php include("inc/header.php"); ?>

<?php
  $site_logo = $this->admin_model->getSetting('SITE_LOGO');
  $site_url = $this->admin_model->getSetting('SITE_URL');
?>
</head>

<body class="texture">

<div id="cl-wrapper" class="login-container">

	<div class="middle-login">
		<div id="response">
        	<?php 
			if(isset($_GET['msg']) && $_GET['msg'] !=''){
				if($_GET['succ']==1){
					echo $this->messages_model->getSuccessMsg($_GET['msg']);
				}
				else if($_GET['succ']==0){
					echo $this->messages_model->getErrorMsg($_GET['msg']);
				}
			}?>
			<?php echo validation_errors('<div class="alert alert-danger alert-white rounded"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
        </div>
		<div class="block-flat">
			<div class="header">							
				<h3 class="text-center"><img class="logo-img" src="<?php echo base_url().'assets/frontend/images/'.$site_logo;?>" alt="logo" style="height:50px;"/></h3>
			</div>
			<div>
				<form class="form-horizontal" action="<?php echo $this->config->config['base_url']; ?>admin" method="post">
					<div class="content">
						<h4 class="title">Login Access</h4>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" placeholder="Username" name="v_name" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" placeholder="Password" name="v_password" class="form-control" required>
									</div>
								</div>
							</div>
							
					</div>
					<div class="foot">
						<button class="btn btn-primary" data-dismiss="modal" type="submit">Login</button>
					</div>
				</form>
			</div>
		</div>
		<div class="text-center out-links"><a href="#">&copy; <?php echo date('Y'); ?></a></div>
	</div> 
</div>
<?php  ?>
