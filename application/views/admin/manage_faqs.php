<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>

<div id="cl-wrapper" class="fixed-menu">
<?php include("inc/sidebar.php"); ?>
    <style>
        .btn.btn-xs{
            color:#ffffff;
        }
    </style>
	<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">
							<h3> 
									
	                        	<?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 
								<?php if($script == 'list'){ ?>
								<a href="<?php echo base_url().'admin/manage_faqs/add/';?>" class="fright">
									<button class="btn btn-primary" type="button">Add <?php  echo ' '.ucfirst($page);?></button>
							    </a> 
							    <?php } ?>
	                        </h3>
						</div>
						<div id="response">
							<?php 
							if(!($this->form_validation->error_array())){
								if(isset($_GET['msg']) && $_GET['msg'] !=''){
									if($_GET['succ']==1){
										echo $this->messages_model->getSuccessMsg($_GET['msg']);
									}
									else if($_GET['succ']==0){
										echo $this->messages_model->getErrorMsg($_GET['msg']);
									}
								}
							}?>
                        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
						</div>
						<?php 
							if(($script == 'add') || ($script == 'edit')){ ?>

							<div class="row">
								<div class="col-md-12">
									<div class="content">
										
										<form role="form" action="" method="post" enctype="multipart/form-data" novalidate="">
    
                                       		<div class="form-group">
                                                <label>Question<span style="color:red;">*</span></label>
                                                <input type="text" placeholder="Question" class="form-control" name="v_question" value="<?php echo $v_question?>" required="required" data-parsley-trigger="keyup">
                                            </div>
                                            <?php $url = base_url().'assets/frontend/css/style.css';?>
                                            <?php $content = $l_answer;?>
                                            <?php /*?><script type="text/javascript">   
                                            	
									            
                                            $(document).ready(function(){
                                            	
                                              CKEDITOR.replace( 'l_answer',{
                                                height: '400px',
                                                toolbar : 'Basic',
												contentsCss : '<?php echo $url;?>',
                                              });

                                            });
                                                       
									        </script>
                                            <div class="form-group">
                                               	<label>Answer<span style="color:red;">*</span></label>
                                                <textarea placeholder="Description" class="form-control" id="l_answer" name="l_answer" required="required" data-parsley-trigger="keyup"><?php echo $l_answer?></textarea>
                                           	</div><?php */?>
                                            <div class="form-group">
                                               	<label>Answer<span style="color:red;">*</span></label>
                                                <textarea placeholder="Description" class="form-control" id="l_answer" name="l_answer" required="required" data-parsley-trigger="keyup"><?php echo $l_answer?></textarea>
                                           	</div>
                                           	<div class="form-group">
	                                            <label>Order</label>
	                                            <input type="text" class="form-control" name="i_order" value="<?php echo $i_order;?>" data-parsley-trigger="keyup" >
	                                        </div>

                                           <div class="form-group">
												<label>Status<span style="color:red;">*</span></label>
												<select class="select2" name="e_status" id="e_status" required>
                                                	<option value="">-</option>
													<?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>
												</select>
											</div>
                                            
                                            <div class="form-group">
                                                    <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?> </button>
                                                    <a href="<?php echo base_url().'admin/manage_faqs/';?>">
                                                    <button class="btn fright" type="button" name="submit_btn">Cancel</button>
                                                    </a> 
                                            </div>
				                   		</form>
									</div>
								</div>
							</div>
						<?php }else{ ?>	

							<div class="row">
								<div class="col-md-12">
									<div class="content">

									<form name="frm_pages" action="<?php echo base_url().'admin/manage_faqs/';?>" method="get">
									  	<div class="table-responsive">
											<div class="row">
												<div class="col-sm-12">
													<div class="dataTables_filter" id="datatable_filter">
														<label>
															<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
															<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>
															<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
														</label>
													</div>
                                                    <div class="pull-left">
                                                        <div id="datatable_length" class="dataTables_length">
                                                            <label>
                                                                <?php $this->paging_model->writeLimitBox(); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="clearfix"></div>
											</div>
										</div>
										<table class="table table-bordered" id="datatable" >
										   	<thead>
										   		<tr>
													<th width="20%">Question</th>
                                                    <th width="40%">Answer</th>
													<th>Date</th>
													<th>order</th>
                                                    <th>Status</th>
													<th></th>
												</tr>
										   	</thead>
										   	<tbody>

										   <?php 
										   	if(!empty($rows)){
										   	foreach ($rows as $key => $val) { ?>
										   		<tr>
													<td><?php echo $val['v_question']; ?></td>
                                                    <td><?php echo $val['l_answer']; ?></td>
													<td><?php echo $val['d_added']; ?></td>
													<td><?php echo $val['i_order']; ?></td>
                                                    <td><?php echo $val['e_status']; ?></td>
													<td>
														<div class="btn-group action_btns">
															<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/manage_faqs/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>

															<?php if($val['e_status'] == 'inactive'){?>
															<a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url().'admin/manage_faqs/active/'.$val["id"].'/';?>"><span class="fa fa-eye"></span></a>
																<?php }else{?>

															<a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url().'admin/manage_faqs/inactive/'.$val["id"].'/';?>"><span class="fa fa-eye-slash"></span></a>
																<?php } ?>

															<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/manage_faqs/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a>
														</div>
													</td>
												</tr>
											<?php } }else{?>

                                                <tr><td colspan="4">No Record found.</td></tr>

                                            <?php }?>
										   </tbody>
										</table>	
										<div class="row">
											<div class="col-sm-12">
												<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>
												<div class="pull-right">
													<div class="dataTables_paginate paging_bs_normal">
														<ul class="pagination">
															<?php $this->paging_model->writePagesLinks(); ?>
														</ul>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />
										<input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />
										<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />
									</form>
								</div>
								
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=oc4n1tsbxfuh9n6g3i0lyaf9tvyxadvpf5l029shmnck6bn8"></script>
<script>
tinymce.init({
  selector: '#l_answer',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  content_css: [
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
</script>

<script type="text/javascript">
$( "#removeImage" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removeimage/'.$v_image;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".dropzone-previews").hide();
	  }
	});
});
 </script>