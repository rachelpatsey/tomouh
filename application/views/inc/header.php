<?php

$url = $_SERVER['REQUEST_URI'];
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$site_url = $this->tomouh_model->getSetting('SITE_URL');
$site_logo = $this->tomouh_model->getSetting('SITE_LOGO');
if(isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])){

  $logged_data = json_decode($_COOKIE['tomouh_logged_data'],true);

  $this->session->set_userdata($logged_data);
}
// $adword_script = $this->tomouh_model->getSetting('ADWORD_SCRIPT');
// $adword_script = $this->tomouh_model->getSetting('ADWORD_SCRIPT');


?>
<div class="main-header">
  <nav class="navbar navbar-fixed-top hidden-xs hidden-sm <?php if($actual_link !== $site_url){ ?> inner_header <?php } ?>" id="menu">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $site_logo; ?>" alt="" class="img-responsive"></a> </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
           <?php $parent_menu = $this->tomouh_model->getParentMenus(); 
           foreach($parent_menu as $menu){
                $child_menu = $this->tomouh_model->getChildMenus($menu['id']);  
           ?>
            <li class="<?php if(isset($child_menu) && !empty($child_menu)){ ?>dropdown<?php } ?> <?php if($menu['v_link'] == $actual_link){ ?> active <?php } ?> <?php if(!empty($child_menu)){ foreach($child_menu as $cm){ if($cm['v_link'] == $actual_link){ ?> active <?php } } } ?>"> 
              <a href="<?php echo $menu['v_link'];?>" <?php if(isset($child_menu) && !empty($child_menu)){ ?> class="dropdown-toggle"  data-toggle="dropdown"  <?php } ?>><?php echo $menu['v_title'];?></a>
                  <?php if(isset($child_menu) && !empty($child_menu)){ ?>
                    <ul class="dropdown-menu">
                      <?php foreach($child_menu as $child){ ?>
                        <?php if($child['v_title'] == 'Apply To Tomouh' && $this->session->has_userdata('logged_user')){?>
                        <?php }else{ ?>    
                        <li><a href="<?php echo $child['v_link'];?>"><?php echo $child['v_title'];?></a></li>
                        <?php } ?>
                      <?php } ?>
                    </ul>
                  <?php } ?>
            </li>  
            <?php } ?>    
           <?php if($this->session->has_userdata('logged_user')){?>
           <li class="login_signup"><a href="<?php echo base_url(); ?>account" class="signup">Account</a><a href="<?php echo base_url(); ?>logout" class="login">Logout</a></li>
           <?php }else{ ?>
            <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Sign Up</a><a href="<?php echo base_url(); ?>login" class="login">Login</a></li>
            <?php } ?>
          </ul>
      </div>
    </div>
  </nav>
<div class="mobile_responsive_menu hidden-lg hidden-md">
    <div class="mobile_logo" id="menu1"> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $site_logo; ?>" alt="" class="img-responsive"></a> </div>
    <header>
      <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>
        <div class="wrapper clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> </div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list clearfix">
            <li class="active"><a href="<?php echo base_url(); ?>">Home<span class="wsmenu-click"><i class="fa fa-close" onClick="hideMenu()"></i></span></a></li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">About Us<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li class=""><a href="<?php echo base_url(); ?>about_us">About Tomouh</a></li>
                <li><a href="<?php echo base_url(); ?>team">Team</a></li>
                <li class=""><a href="<?php echo base_url(); ?>honorary_member">Honorary Members</a></li>
                <li><a href="<?php echo base_url(); ?>membership_committee">Membership Committee</a></li>
                <li><a href="<?php echo base_url(); ?>sponsorship">Sponsorship</a></li>
              </ul>
            </li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">Our Members<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li class=""><a href="<?php echo base_url(); ?>founders">Founders</a></li>
                <!-- <li><a href="<?php echo base_url(); ?>tcl_fellow">TCL fellowship</a></li> -->
             </ul>
            </li>
          <!--  <li><a href="why_join.html">Why Join</a></li>-->
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">Membership<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li><a href="<?php echo base_url(); ?>membership_process">Membership Process</a></li>
                <li><a href="<?php echo base_url(); ?>signup">Apply to Tomouh</a></li>
                <li class=""><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
              </ul>
            </li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">The Latest<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li class=""><a href="<?php echo base_url(); ?>media">Media</a></li>
                <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>event">Events</a></li>
            <li class=""><a href="<?php echo base_url(); ?>contact_us">Contact</a></li>
            <?php if($this->session->has_userdata('logged_user')){?>
              <li class="login_signup"><a href="<?php echo base_url(); ?>account" class="signup">Account</a></li>
              <li class="login_signup"><a href="<?php echo base_url(); ?>logout" class="signup">Logout</a></li>
            <?php }else{ ?>
              <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Sign Up</a></li>
              <li class="login_signup"><a href="<?php echo base_url(); ?>login" class="signup">Login</a></li>
            <?php } ?>
          </ul>
        </nav>
      </div>
    </header>
  </div>
</div>