<!doctype html>

<html>

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>/favicon.png">
<meta name="description" content="<?php echo $meta_description; ?>">

<meta name="keyword" content="<?php echo $meta_keyword; ?>">

<title><?php echo $meta_title;?> </title>

<!-- Bootstrap Core CSS -->

<link href="<?php echo base_url(); ?>assets/client/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/client/css/webapplinks.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.theme.default.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.carousel.css">

<link href="<?php echo base_url(); ?>assets/client/css/bootstrap-select.min.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>assets/client/css/animated.css" rel="stylesheet">

<!-- Custom CSS -->

<link href="<?php echo base_url(); ?>assets/client/css/style.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/font-awesome.min.css">



<script src="<?php echo base_url(); ?>assets/client/js/jquery.js"></script> 

<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>

<script src="<?php echo base_url(); ?>assets/client/js/bootstrap.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/client/js/jquery-mobile.js"></script>

<!-- Adword Script  -->

<?php

	$adword_script = $this->tomouh_model->getSetting('ADWORD_SCRIPT');

	echo $adword_script;

 ?>

</head>

<body>