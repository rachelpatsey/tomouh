<?php 

require_once('eventbrite/HttpClient.php');

$EVENTBRIGHT_TOKEN = $this->tomouh_model->getSetting('EVENTBRIGHT_TOKEN');
$events  = $this->tomouh_model->getEvents();


if(isset($events) && !empty($events)){
  foreach($events as $event){
    
    $event_month = date('M', strtotime($event['d_datetime']));

    $tomouh_events[$event_month][]=$event;

  }

}
/* page description  >> */
$result = $this->tomouh_model->getPageBySlug('event');
$data = $result;   
$main_description = $result['l_description'];
$title = $result['v_title'];

/* << page description  >> */
// $tomouh_events = [];
include("inc/header_top.php");
include("inc/header.php"); 
?>
<style type="text/css">
  #response{
    margin-top: 40px;
  }
  strong {
    font-weight: 600 !important;
  }
</style>
<div class="inner_wrapper">

    <?php
    if(!($this->form_validation->error_array())){
      if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
      <div id="response" class="error_box">
      <?php  if($_GET['succ']==1){

          echo $this->messages_model->getSuccessMsgEvent($_GET['msg']);
        }
        else if($_GET['succ']==0){

          echo $this->messages_model->getErrorMsg($_GET['msg']);
        } ?>
    </div>
    <?php  }
    }?>

  <div class="inner_header_div">

    <div class="container">

      <div class="row">

        <div class="col-sm-7 col-md-8 col-xs-12">

          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event" class="active">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>

        </div>

        <div class="col-sm-5 col-md-4  col-xs-12">

          <div class="right-div"> <a href="<?php echo base_url(); ?>account" >Account</a> <a href="<?php echo base_url(); ?>profile" >Profile</a> </div>

        </div>

      </div>

    </div>

  </div>

  <div class="event_page">

    <section class="event_cal">

      <div class="sec_banner">

        <div class="container">          
          <div class="page_head">
            <h1 class="red-text text-center font36"><?php if (isset($title)) {echo $title;} ?></h1>
            <p class="content"><?php if (isset($main_description)) {echo $main_description;} ?></p><br><br>
          </div>

          <div class="month_list">

            <ul>

              <?php 
			  $today_month = date('n');
			  $start_date = date('Y-m-d', strtotime('-3 months'));	
			  $current_month = date('n', strtotime($start_date));
        $current_date = date('Y-m-d');

			  $current_year = date('Y', strtotime($start_date));

			  $to_month = $current_month+6;

			  

			  for ($monthIndex=$current_month; $monthIndex <= $to_month; $monthIndex++) {

				 $monthNew = $monthIndex;

				 $class_name ='';

				 

				 if($monthNew > 12) $monthNew = $monthNew-12;

				 if($today_month == $monthNew) $class_name ='active';

				 

				 $monthStr = date('M', mktime(0,0,0,$monthNew, 1, date('Y')));

				 echo '<li><a class="'.$class_name.'" href="javascript:;" data-id="'.$monthNew.'">'.$monthStr. '</a></li>';

			  }

			  ?>	

            </ul>

          </div>

          
          <?php 

		  	 for ($monthIndex=$current_month; $monthIndex <= $to_month; $monthIndex++) {

				 $monthNew = $monthIndex;

				 $class_name ='';

				 

				 if($monthNew > 12) $monthNew = $monthNew-12;

				 if($today_month == $monthNew) $class_name ='show';

				 

				 $monthStr = date('M', mktime(0,0,0,$monthNew, 1, date('Y')));

				 ?>

                 <div class="list_block block-<?php echo $monthNew;?> <?php echo $class_name;?>" >

                 	<?php if(isset($tomouh_events[$monthStr]) && !empty($tomouh_events[$monthStr])){

						  	foreach($tomouh_events[$monthStr] as $tomouh_event){

							$event_date = date('Y-m-d', strtotime($tomouh_event['d_datetime']));	
							$end_date = date('Y-m-d', strtotime($tomouh_event['d_datetime']));	
							// echo "<pre>"; print_r($tomouh_event);
							
					?>

                    		<div class="list-box">

                              <div class="col-sm-12 col-xs-12">

                                <div class="row">

                                  <div class="col-sm-3 col-md-3 col-xs-12 red-box">

                                    <div class="date_block">

                                      <h1 class="date">
                                      <span>
									  <?php 
									  if($event_date != $end_date)
									  echo date('d M', strtotime($event_date)).' - '.date('d M', strtotime($end_date));
									  else
									  echo date('d M', strtotime($event_date));
									  	  
									  
									  ?>
                                      </span></h1>

                                      <p class="day"><?php 
									  if($event_date != $end_date)
										echo date('D', strtotime($event_date)).' - '.date('D', strtotime($end_date));
									  else
									 	echo date('D', strtotime($event_date));   	
										
									  ?> 
                                      </p>

                                    </div>

                                  </div>

                                  <div class="col-sm-9 col-md-9 col-xs-12 no-padding">

                                    <div class="content_block">

                                      <div class="col-sm-10 col-xs-12">

                                        <h1 class="event_name red-text" id="e_name_<?php echo $tomouh_event['id']?>"><strong><?php echo $tomouh_event['t_title']?></strong></h1>

                                        <?php if($this->session->has_userdata('logged_user')){?>

                                        <div class="row new_row">

                                          <div class="col-xs-3 col-sm-3 col-md-2">

                                            <p class="light-gray-text">time</p>

                                            <p><?php echo date('h:i A', strtotime($tomouh_event['d_datetime']));?></p>

                                          </div>

                                          <div class="col-xs-9 col-sm-9 col-md-10">

                                            <p class="light-gray-text">venue</p>

                                            <p><?php echo $tomouh_event['v_city']?></p>

                                          </div>

                                        </div>

                                        <?php }?>

                                        

                                      </div>

                                      <div class="col-sm-2 col-xs-12">

                                      <?php if($this->session->has_userdata('logged_user')){

                                        if($event_date < $current_date ){
                                          $disable_cls = "disabled";
                                        }else{
                                          $disable_cls ="";
                                        }
                                      ?>

                                        

                                      <a href="javascript:;" onclick="fn_open_model_rsvp(this.id)" id="<?php echo $tomouh_event['id']; ?>">

                                        <button type="button" class="btn gray-btn white-text pull-right dark-gray" <?php echo $disable_cls;?>>RSVP</button>

                                      </a>
                                    
                                      <?php }?>

                                      </div>

                                    </div>

                                  </div>

                                </div>

                              </div>

                            </div>

                    <?php			

							}

						  

						  }else{?>

                    	<p style="text-align:center;">No events found.</p>

					<?php }?>

                 </div>

                 

                 <?php

				 

			  }

		  

		  ?>

          

        </div>

      </div>

    </section>

  </div>

</div>

<!-- Modal HTML -->

<div id="myModal" class="modal fade">

  <div class="modal-dialog video_modal">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Tomouh Q&A with HRH Prince Turki Al Faisal</h4>

      </div>

      <div class="modal-body">

        <div class="videoWrapper">

          <iframe src="https://www.youtube.com/embed/wYUviw6T6W4" allowfullscreen="" width="640" height="480" frameborder="0"></iframe>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="rsvp_model" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <font color="black" >All * fields are mandatory</font>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        </div>
        <div class="modal-body">
          <div id="user-detail">
            <form action="" method="post" parsley-validate>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="message" class="form-control-label">Subject:<span style="color:red;">*</span></label>
                    <br>
                    <input class="form-control" type="text" name="subject" id="subject" value="" required="required" readonly="readonly">
                    <input type="hidden" name="event_id" id="event_id">
                    <input type="hidden" name="member_email" id="member_email">
                  </div>
                  <div class="form-group">
                    <label for="message" class="form-control-label">Message:<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="5" id="message" name="message" required="required"></textarea>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" name="send_mail" value="send_mail" class="btn btn-primary" style="background: #800000;border-color: #800000;">Send</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
   </div>
</div>

<?php 

include("inc/footer.php"); 

?>

<style type="text/css">

.event_page .list_block {display:none};

</style>

<script type="text/javascript">

jQuery(document).ready(function(e) {

   jQuery('.month_list ul li a').click(function(){

   		var data_id = jQuery(this).attr('data-id');

		jQuery('.event_page .list_block').removeClass('show');

		jQuery('.event_page .list_block.block-'+data_id).addClass('show');

		

		jQuery('.month_list ul li a').removeClass('active');

		jQuery(this).addClass('active');

		

   });

});	

function fn_open_model_rsvp(id) {
  var event_name = jQuery('#e_name_'+id).text();
  jQuery('#subject').val('RSVP: '+event_name);
  jQuery('#event_id').val(id);
  jQuery('#rsvp_model').modal('show');
}

</script>