<?php
include('inc/header_top.php');
include('inc/header.php');
?>
    <style type="text/css">
      .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
      .error_box .icon{float: left;padding-right: 10px;}
    </style>   
<div class="inner_wrapper">
              <?php 
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                <div id="response" class="error_box">
              <?php    if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
                </div>
              <?php  }
              }?>
              <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
<div class="container">
    <div class="login_page">
        <div class="center-div">
            <div class="logo" style="margin-top:10px"><img src="<?php echo base_url(); ?>assets/frontend/images/logo-2.png" alt="" class="images-responsive"></div>
            <div class="login-box" style="margin-top: 0px;">
                <form action="" method="post" class="form"> 
                    <h2 class="login-head red-text spectral-font font36 text-center"><strong>Sign in</strong></h2>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                      <input type="checkbox" name="keep_logged_in" id="keep_logged_in">
                      <label>Keep Me Logged In</label>
                    </div>
                    <div class="form-group">
                        <!-- <a href="" class="btn red-btn">Login</a> -->
                        <input type="submit" name="login" value="Login" class="btn red-btn">
                       <!-- <button class="btn red-btn" type="button">Login</button>-->
                    </div>
                    <div class="form-footer">
                        <p class="not_member">Not a member? <a href="<?php echo base_url(); ?>signup">Sign up</a></p>
                        <p class="re_pwd"><a href="<?php echo base_url(); ?>forgot_password" style="color:#343434;">Recover my password</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
 </div>
    </div>
<?php include('inc/footer.php'); ?>