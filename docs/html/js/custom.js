// carousel swipe
//Select Menu  Color Script
$(document).ready(function() {
    $(".red, .pink, .purple, .deep-purple, .indigo, .blue, .light-blue, .cyan, .teal, .green, .light-green, .lime, .yellow, .amber, .orange, .deep-orange, .brown, .white, .blue-grey, .black").on("click", function() {
        $(".wsmenu")
            .removeClass()
            .addClass('wsmenu pm_' + $(this).attr('class') );       
    });
	$(".toggle").on("click", function() {
         $(".pop-up").toggleClass('inactive');     
    });
    $('#wsnavtoggle').click(function(){
                   $('body').css('overflow','hidden'); 
                });
                
                $('#teacher_drop').click(function(){
						    	if($(this).hasClass('wsmenu-rotate')){
										$(this).removeClass('wsmenu-rotate');						    	
						    	}else{
						    	$(this).addClass('wsmenu-rotate');
						    	}            
                });
               
});
//Select Menu  Color Script

( function( window ) {
'use strict';
function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 0,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
	function hideMenu()
	{
		$(".wsmenucontainer.clearfix").removeClass("wsoffcanvasopener");
		$('body').css('overflow','auto');
		}




//youtube video


// Find all YouTube videos
var $allVideos = $("iframe[src^='//www.youtube.com']"),

    // The element that is fluid width
    $fluidEl = $("body");

// Figure out and save aspect ratio for each video
$allVideos.each(function() {

  $(this)
    .data('aspectRatio', this.height / this.width)

    // and remove the hard coded width/height
    .removeAttr('height')
    .removeAttr('width');

});

// When the window is resized
$(window).resize(function() {

  var newWidth = $fluidEl.width();

  // Resize all videos according to their own aspect ratio
  $allVideos.each(function() {

    var $el = $(this);
    $el
      .width(newWidth)
      .height(newWidth * $el.data('aspectRatio'));

  });

// Kick off one resize to fix all videos on page load
}).resize();



/* Slider jquery */

  $(document).ready(function() {  
     $("#myCarousel").swiperight(function() {  
        $(this).carousel('prev');  
       });  
     $("#myCarousel").swipeleft(function() {  
        $(this).carousel('next');  
    });  
 }); 

$(document).ready(function() {

  //Owl Carousel  Start 
      $("#owl-demo").owlCarousel({
        margin:15,
        dots:false,
        loop :true,
        autoplay:true,
        slideSpeed:500, 
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }     
        }
      });

      // Custom Navigation Events
      $("#prev").click(function () {
        $("#owl-demo").trigger('prev.owl.carousel');
      });

      $("#next").click(function () {
        $("#owl-demo").trigger('next.owl.carousel');
      });  
  //Owl Carousel  End  

});

  /* Slider jquery */

  $(document).ready(function() {  
     $("#myCarousel").swiperight(function() {  
        $(this).carousel('prev');  
       });  
     $("#myCarousel").swipeleft(function() {  
        $(this).carousel('next');  
    });  
 }); 


/*team-popup*/

$('#img-click').on('click', function(){
    $('#pop-up').toggleClass('open');
    
});
$('#img-close').on('click', function(){
    $('#pop-up').removeClass('open');
});


$('#img-click1').on('click', function(){
    $('#pop-up1').addClass('open');
    
});
$('#img-close1').on('click', function(){
    $('#pop-up1').removeClass('open');
});

$('#img-click2').on('click', function(){
    $('#pop-up2').addClass('open');
    
});
$('#img-close2').on('click', function(){
    $('#pop-up2').removeClass('open');
});


$('#img-click3').on('click', function(){
    $('#pop-up3').addClass('open');
    
});
$('#img-close3').on('click', function(){
    $('#pop-up3').removeClass('open');
});


$('#img-click4').on('click', function(){
    $('#pop-up4').addClass('open');
    
});
$('#img-close4').on('click', function(){
    $('#pop-up4').removeClass('open');
});
$('#img-click5').on('click', function(){
    $('#pop-up5').addClass('open');
    
});

$('#img-close5').on('click', function(){
    $('#pop-up5').removeClass('open');
});



$('.img-click').on('click', function(){
     document.getElementById("pop-overlay").style.display = "block";
});
$('.close-img').on('click', function(){
     document.getElementById("pop-overlay").style.display = "none";
});





/*vertical slider js*/

$(document).ready(function () {

    $('.btn-vertical-slider').on('click', function () {
        
        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel1').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel1').carousel('prev')
        }

    });
});

/*faq js*/
//Accordian Action
var action = 'click';
var speed = "500";


//Document.Ready
$(document).ready(function(){
  //Question handler
$('li.q').on(action, function(){
  //gets next element
  //opens .a of selected question
$(this).next().slideToggle(speed)
    //selects all other answers and slides up any open answer
    .siblings('li.a').slideUp();
  
  //Grab img from clicked question
var img = $(this).children('.img');
  //Remove Rotate class from all images except the active
  $('.img').not(img).removeClass('rotate');
  //toggle rotate class
  img.toggleClass('rotate');

});//End on click




});//End Ready



